/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-12-31 09:21:18
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-31 09:24:37
 */

/// 版本比对
/// 返回 0 版本相同， 返回1 v1> v2 返回 -1 v1<v2
int  compareVersion({required String v1,required String v2}) {
  List<String> v1Arr = v1.split('.');
  List<String> v2Arr = v2.split('.');
  var minVersionLens =
      v1Arr.length > v2Arr.length ? v2Arr.length : v1Arr.length;
  var result = 0;
  for (int i = 0; i < minVersionLens; i++) {
    var curV1 = int.parse(v1Arr[i]);
    var curV2 = int.parse(v2Arr[i]);
    if (curV1 > curV2) {
      result = 1;
      break;
    } else if (curV1 < curV2) {
      result = -1;
      break;
    }
  }
  if (result == 0 && (v1Arr.length != v2Arr.length)) {
    var v1BiggerThenv2 = v1Arr.length > v2Arr.length;
    var maxLensVersion = v1BiggerThenv2 ? v1Arr : v2Arr;
    for (int i = minVersionLens; i < maxLensVersion.length; i++) {
      var curVersion = int.parse(maxLensVersion[i]);
      if (curVersion > 0) {
        v1BiggerThenv2 ? (result = 1) : (result = -1);
        break;
      }
    }
  }
  return result;
}
