/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-12-30 14:30:19
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-30 21:22:51
 */

/// 判断是否是网络资源
bool isNetResource(String url) {
  RegExp urlreg = RegExp(r"^((https|http|ftp|rtsp|mms)?:\/\/)[^\s]+");
  return urlreg.hasMatch(url);
}
