/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-12-01 17:43:47
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-01 22:17:19
 */

/// 毫秒数处理，返回一定格式字符串
class TimerFormat {
  final String? year;
  final String? month;
  final String? day;
  final String? hour;
  final String? minute;
  final String? second;
  final bool istrunc;
  TimerFormat(
      {this.year,
      this.month,
      this.day,
      this.hour,
      this.minute,
      this.second,
      this.istrunc = false});
  factory TimerFormat.format(int timer) {
    return TimerFormat();
  }

  /// 毫秒数处理，返回一定格式字符串
  static String formatData(int timer,
      [String format = 'yyyy-MM-dd HH:mm:ss', bool istrunc = false]) {
    const yearConst = 365 * 24 * 60 * 60;

    const monthConst = 30 * 24 * 60 * 60;

    const dayConst = 24 * 60 * 60;

    const hourConst = 60 * 60;
    String template = format;

    int interval = timer;

    int year = 0, month = 0, day = 0, hour = 0, minute = 0, second = 0;

    /// 如果要显示年份
    if (format.startsWith('yyyy')) {
      year = interval ~/ yearConst;
    }

    /// 如果要显示月份
    if (format.contains('MM')) {
      interval -= year * yearConst;
      month = interval ~/ monthConst;
    }

    /// 如果要显示天数
    if (format.contains('dd')) {
      interval -= month * monthConst;

      /// 不存在时分秒，向上取整
      if (!(format.contains('HH') &&
              format.contains('mm') &&
              format.contains("ss")) &&
          istrunc) {
        day = (interval / dayConst).ceil();
      } else {
        day = interval ~/ dayConst;
      }
    }

    /// 如果要显示小时
    if (format.contains('HH')) {
      interval -= day * dayConst;
      //  如果不存在分跟秒，向上取整
      if (!(format.contains('mm') && format.contains("ss"))) {
        hour = (interval / hourConst).ceil();
      } else {
        hour = interval ~/ hourConst;
      }
    }

    /// 如果要显示分钟
    if (format.contains('mm')) {
      interval -= hour * hourConst;
      minute = interval ~/ 60;
    }

    /// 如果要显示秒
    if (format.contains('ss')) {
      second = interval % 60;
    }

    return template
        .replaceAll('yyyy', year.toString().padLeft(4, '0'))
        .replaceAll('MM', month.toString().padLeft(2, '0'))
        .replaceAll('dd', day.toString().padLeft(2, '0'))
        .replaceAll('HH', hour.toString().padLeft(2, '0'))
        .replaceAll('mm', minute.toString().padLeft(2, '0'))
        .replaceAll('ss', second.toString().padLeft(2, '0'));
  }
}
