/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-12-21 15:51:26
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-31 09:28:57
 */
import 'package:flutter/cupertino.dart';

/// 打印输出
class PrintMore {
  static int step = 120;

  static log(dynamic data) {
    String template = data.toString();
    int currentIndex = 0;

    do {
      int max = template.length > (currentIndex + 1) * step
          ? (currentIndex + 1) * step
          : template.length;
      debugPrint(
          'print------->' + template.substring(currentIndex * step, max));
      currentIndex++;
    } while (currentIndex * step <= template.length);
  }
}
