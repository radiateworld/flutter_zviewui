library zviewui_utils;

export 'is_net_resource.dart';
export 'print.dart';
export 'global_currency.dart';
export 'color_thief_flutter/index.dart';
export 'adapter.dart';
export 'net_jump.dart';
export 'timer_format.dart';
export 'tab_indicator.dart';
export 'test.dart';
export 'timer_format.dart';
export 'type_conversion.dart';
export 'version_comparison.dart';

/// 公共方法
class ZviewUtils {
  ///记录点击时间
  static final Map<String, DateTime?> lastPopTime = {};

  // 防重复提交,当一个页面有多个按钮时需要设置index值
  static bool checkClick({int needTime = 2000, String index = '000'}) {
    if (lastPopTime[index] == null ||
        DateTime.now().difference(lastPopTime[index]!) >
            Duration(milliseconds: needTime)) {
      lastPopTime.addAll({
        index: DateTime.now(),
      });
      return true;
    }
    // MyToast(S.current.againlater);
    return false;
  }

  static checkClickClear() {
    lastPopTime.clear();
  }
}
