/*
 * @Descripttion: 
 * @version: 
 * @Author: 黄志勇
 * @Date: 2021-08-02 22:39:06
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-10-25 11:51:16
 */
import 'package:flutter/material.dart'
    show BuildContext, MediaQuery, MediaQueryData;
import 'dart:ui';

class Adapter {
  static final MediaQueryData mediaQuery = MediaQueryData.fromWindow(window);

  static const double size = 750;
  static final double _topbarH = mediaQuery.padding.top;
  static final double _botbarH = mediaQuery.padding.bottom;
  static final double _pixelRatio = mediaQuery.devicePixelRatio;
  // Adapter({this.size = 750});

  // 设置尺寸
  static double rpx(double value) {
    return count() * value;
  }

  // 返回像素比
  static double count() {
    return mediaQuery.size.width / size;
  }

  static double pixelRatio() {
    return _pixelRatio;
  }

  static double navbarHeight() {
    return _topbarH;
  }

  static double bottomHeght() {
    return _botbarH;
  }
}

extension Int2Rpx on int {
  double get $rpx => Adapter.rpx(toDouble());
}

extension DOuble2Rpx on double {
  double get $rpx => Adapter.rpx(this);
}

extension MediaQueryExtension on BuildContext {
  Size get size => MediaQuery.of(this).size;
  double get height => MediaQuery.of(this).size.height;
  double get width => MediaQuery.of(this).size.width;
}
