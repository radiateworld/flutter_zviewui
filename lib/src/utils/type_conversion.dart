/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-12-22 20:42:04
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-27 19:39:13
 */

/// 类型转换
class TypeConversion {
  /// Map => Map<String,dynamic>
  static Map<String, dynamic> mapToMapStringDynamic(Map data) {
    Map<String, dynamic> template = {};
    for (int i = 0; i < data.keys.length; i++) {
      template[data.keys.toList()[i].toString()] = data[data.keys.toList()[i]];
    }
    return template;
  }

  /// List< Map > => List<Map<String,dynamic>>
  static List<Map<String, dynamic>> listMapToListMapStringDynamic(
      List<Map> data) {
    List<Map<String, dynamic>> template = [];
    for (int i = 0; i < data.length; i++) {
      template.add(TypeConversion.mapToMapStringDynamic(data[i]));
    }
    return template;
  }

  /// List => List<Map<String,dynamic>>
  static List<Map<String, dynamic>> listToListMapStringDynamic(List data) {
    List<Map<String, dynamic>> template = [];
    for (int i = 0; i < data.length; i++) {
      template.add(TypeConversion.mapToMapStringDynamic(data[i]));
    }
    return template;
  }

  /// List => List<String>
  static List<String> listToListString(List data) {
    List<String> template = [];
    for (int i = 0; i < data.length; i++) {
      template.add(data[i].toString());
    }
    return template;
  }
}
