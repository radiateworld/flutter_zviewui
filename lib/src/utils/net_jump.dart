import 'package:flutter/cupertino.dart';

netJump({required String url, required BuildContext context, int state = 1}) {
  Uri uri = Uri.parse(url);

  /// 有参数
  if (uri.queryParameters.isNotEmpty) {
    switch (state) {

      /// 关闭当前页面跳转下一页
      case 2:
        Navigator.pushReplacementNamed(context, uri.path,
            arguments: uri.queryParameters);
        break;

      /// 关闭所有页面跳转到指定页面
      case 3:
        Navigator.pushNamedAndRemoveUntil(context, uri.path, (route) {
          return false;
        }, arguments: uri.queryParameters);
        break;

      /// 关闭所有页面跳转到tab页面
      case 4:
        Navigator.pushNamedAndRemoveUntil(context, "/", (route) {
          return true;
        }, arguments: uri.queryParameters);
        break;

      /// 默认跳转到下一页
      default:
        Navigator.pushNamed(context, uri.path, arguments: uri.queryParameters);
      // assert(false, '跳转类型不对');
    }

    /// 没有参数
  } else {
    switch (state) {

      /// 关闭当前页面跳转下一页
      case 2:
        Navigator.pushReplacementNamed(
          context,
          uri.path,
        );
        break;

      /// 关闭所有页面跳转到指定页面
      case 3:
        Navigator.pushNamedAndRemoveUntil(
          context,
          uri.path,
          (route) {
            return false;
          },
        );
        break;

      /// 关闭所有页面跳转到tab页面
      case 4:
        Navigator.pushNamedAndRemoveUntil(
          context,
          "/",
          (route) {
            return true;
          },
        );
        break;

      /// 默认跳转到下一页
      default:
        Navigator.pushNamed(
          context,
          uri.path,
        );
      // assert(false, '跳转类型不对');
    }
  }
}
