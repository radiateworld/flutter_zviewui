import './adapter.dart';
import 'package:flutter/material.dart';
import 'package:synchronized/synchronized.dart';

class GlobalCurrency {
  /// 记录GlobalCurrency对象信息
  static GlobalCurrency? _GlobalCurrency;

  /// 是否显示一元夺宝
  static bool isTreasure = false;

  /// 货币单位 后
  static String? suffixUnit;

  /// 货币单位 前
  static String? prefixUnit;
  static final Lock _lock = Lock();
  static Future init(Future<Map<String, dynamic>> Function() getConfig,
      [Future Function()? next]) async {
    _lock.synchronized(() async {
      Map resdata = await getConfig();
      // _GlobalCurrency = GlobalCurrency._();
      isTreasure = resdata['showTreasure'] as bool;
      if (resdata['currencyInfo']['position'] == 'left') {
        prefixUnit = resdata['currencyInfo']['symbol'];
        suffixUnit = '';
      } else {
        prefixUnit = '';
        suffixUnit = resdata['currencyInfo']['symbol'];
      }

      /// 接口的下一步
      if (next != null) {
        next();
      }
    });
  }

  /// 暂时未知其他作用
  GlobalCurrency._();

  static String getPrefix() {
    if ((prefixUnit == null && suffixUnit == null) ||
        (prefixUnit!.isNotEmpty && suffixUnit!.isNotEmpty)) {
      return '￥';
    }
    return prefixUnit ?? '';
  }

  static String getSuffix() {
    return suffixUnit ?? '';
  }
}

extension GlobalCurrencyExtension on String {
  ///  给货币价格添加前缀，后缀
  String get price => this == '' || double.parse(this) == 0
      ? ''
      : GlobalCurrency.getPrefix() + this + GlobalCurrency.getSuffix();

  String get nullPrice => this == ''
      ? ''
      : GlobalCurrency.getPrefix() + this + GlobalCurrency.getSuffix();

  ///构建价格部件
  ///```dart
  ///TextStyle? unitStyle = TextStyle(
  ///   fontSize: 24.$rpx,
  ///   height: 1,
  ///   color: Config.black,
  ///    fontWeight: FontWeight.bold,
  ///  ).merge(unitStyle);
  ///  TextStyle? priceStyle = TextStyle(
  ///   fontSize: 34.$rpx,
  ///   color: Config.black,
  ///    fontWeight: FontWeight.bold,
  ///    height: 1,
  ///  ).merge(priceStyle);
  ///
  ///```
  Widget buildPrice({
    required BuildContext context,
    TextStyle? unitStyle,
    TextStyle? priceStyle,
    double space = 0,
  }) {
    TextStyle? _unitStyle = TextStyle(
      fontSize: 24.$rpx,
      height: 1,
      color: Theme.of(context).textTheme.headline3?.color,
      fontWeight: FontWeight.bold,
    ).merge(unitStyle);
    TextStyle? _priceStyle = TextStyle(
            fontSize: 34.$rpx,
            color: Theme.of(context).textTheme.headline4?.color,
            fontWeight: FontWeight.bold,
            height: 1
            // height: 0.7,
            )
        .merge(priceStyle);

    /// 价格为空
    if (isEmpty) {
      return const SizedBox.shrink();
    }
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          GlobalCurrency.getPrefix(),
          style: _unitStyle,
        ),
        SizedBox(
          width: space,
        ),
        Text(
          this,
          style: _priceStyle,
        ),
        SizedBox(
          width: space,
        ),
        Text(
          GlobalCurrency.getSuffix(),
          style: _unitStyle,
        ),
      ],
    );
  }
}
