/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-16 11:53:35
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-27 10:13:00
 */
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:zviewui/src/utils/adapter.dart';

enum TimelineType {
  row,
  column,
}

class TimeLineWidget extends StatelessWidget {
  final List list;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? margin;
  final TimelineType type;
  final Widget? dot;
  final Widget? activeDot;
  final TextStyle? contentStyle;
  final TextStyle? contentActiveStyle;
  final TextStyle? timeStyle;
  final TextStyle? timeActiveStyle;

  final ScrollPhysics? physics;
  const TimeLineWidget(
      {Key? key,
      required this.list,
      this.padding,
      this.margin,
      this.type = TimelineType.column,
      this.dot,
      this.activeDot,
      this.contentStyle,
      this.contentActiveStyle,
      this.timeStyle,
      this.timeActiveStyle,
      this.physics = const AlwaysScrollableScrollPhysics()})
      : super(key: key);

  BuildContext get context => context;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      margin: margin,
      color: Colors.white,
      child: ListView.builder(
          physics: physics,
          shrinkWrap: true,
          scrollDirection:
              type == TimelineType.row ? Axis.horizontal : Axis.vertical,
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            switch (type) {
              case TimelineType.row:
                return timeLineWidgetRow(context, list, index);
              default:
                return timeLineWidgetColumn(context, list, index);
            }
          }),
    );
  }

  ///水平方向
  Widget timeLineWidgetRow(BuildContext context, List list, int index) {
    return Container();
  }

  /// 标记点
  Widget get _dot =>
      dot ??
      Transform.rotate(
        angle: pi / 4,
        child: Container(
          color: Colors.transparent,
          width: 32.$rpx,
          height: 32.$rpx,
          child: Center(
            child: Container(
              width: 16.$rpx,
              height: 16.$rpx,
              color: Config.grey4,
            ),
          ),
        ),
      );
  Widget get _activeDot =>
      activeDot ??
      Transform.rotate(
        angle: pi / 4,
        child: Container(
          color: Config.theme.withOpacity(0.6),
          width: 32.$rpx,
          height: 32.$rpx,
          child: Center(
            child: Container(
              width: 16.$rpx,
              height: 16.$rpx,
              color: Theme.of(context).primaryColor,
            ),
          ),
        ),
      );

  /// 默认文本样式
  TextStyle get _timeStyle => TextStyle(
          fontSize: 28.$rpx,
          color: Config.grey2,
          height: 40 / 28,
          fontWeight: FontWeight.w400)
      .merge(timeStyle);

  TextStyle get _timeActiveStyle => TextStyle(
          fontSize: 28.$rpx,
          color: Config.black4,
          height: 40 / 28,
          fontWeight: FontWeight.w400)
      .merge(timeActiveStyle);

  /// 默认文本样式
  TextStyle get _contentStyle => TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 26.$rpx,
        color: Config.black4,
      ).merge(contentStyle);

  TextStyle get _contentActiveStyle => TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 26.$rpx,
        color: Config.black4,
      ).merge(contentActiveStyle);

  /// 垂直方向
  Widget timeLineWidgetColumn(BuildContext context, List list, int index) {
    var item = list[index];
    bool active = item['c'] == 'Israel';
    return IntrinsicHeight(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 32.$rpx),
        constraints: BoxConstraints(minHeight: 130.$rpx),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            // 时间
            SizedBox(
              width: 86.$rpx,
              child: Text.rich(
                TextSpan(children: [
                  TextSpan(
                      text: item['a'].split(' ')[0].split('-')[1] +
                          '-' +
                          item['a'].split(' ')[0].split('-')[2],
                      style: active ? _timeActiveStyle : _timeStyle),
                  const TextSpan(text: '\n'),
                  TextSpan(
                      text: item['a'].split(' ')[1].split(':')[0] +
                          ':' +
                          item['a'].split(' ')[1].split(':')[1],
                      style: TextStyle(
                        fontSize: 22.$rpx,
                        color: active ? Config.black4 : Config.grey4,
                        fontWeight: FontWeight.w400,
                      ))
                ]),
                textAlign: TextAlign.center,
              ),
            ),
            // 点跟线
            Container(
              width: 80.$rpx,
              padding: EdgeInsets.only(right: 10.$rpx),
              child: Stack(
                alignment: Alignment.topCenter,
                children: [
                  Container(
                    margin: active
                        ? EdgeInsets.only(top: 36.$rpx)
                        : index == 0
                            ? EdgeInsets.only(top: 20.$rpx)
                            : null,
                    height: index != list.length - 1 ? null : 20.$rpx,
                    child: VerticalDivider(
                      width: 1,
                      color: Theme.of(context).dividerColor,
                    ),
                  ),

                  // 标记点()
                  Positioned(
                    top: 0,
                    child: Container(
                      child: active ? _activeDot : _dot,
                    ),
                  ),
                ],
              ),
            ),
            //内容
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.only(left: 20.$rpx),
                child: Text(
                  item['z'],
                  // maxLines: -1,
                  style: active ? _contentActiveStyle : _contentStyle,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  // double getHeight(String content) {
  //   //具体多长的文字需要增加高度，看手机分辨率和margin、padding的设置了
  //   if (content.length >= 95) {
  //     return 150;
  //   } else if (content.length >= 80 && content.length < 95) {
  //     return 130;
  //   } else if (content.length >= 40 && content.length < 80) {
  //     return 110;
  //   } else if (content.length >= 20 && content.length < 40) {
  //     return 90;
  //   } else {
  //     return 70;
  //   }
  // }

  // //把int类型的状态转成字符串，具体对应请看阿里云API购买页面，本博最后的图也会有
  // String statusConvert(int status) {
  //   String returnStatus;
  //   switch (status) {
  //     case -1:
  //       {
  //         returnStatus = "待查询";
  //       }
  //       break;
  //     case 0:
  //       {
  //         returnStatus = "查询异常";
  //       }
  //       break;
  //     case 1:
  //       {
  //         returnStatus = "暂无记录";
  //       }
  //       break;
  //     case 2:
  //       {
  //         returnStatus = "在途中";
  //       }
  //       break;
  //     case 3:
  //       {
  //         returnStatus = "派送中";
  //       }
  //       break;
  //     case 4:
  //       {
  //         returnStatus = "已签收";
  //       }
  //       break;
  //     case 5:
  //       {
  //         returnStatus = "用户拒签";
  //       }
  //       break;
  //     case 6:
  //       {
  //         returnStatus = "疑难件";
  //       }
  //       break;
  //     case 7:
  //       {
  //         returnStatus = "无效单";
  //       }
  //       break;
  //     case 8:
  //       {
  //         returnStatus = "超时单";
  //       }
  //       break;
  //     case 9:
  //       {
  //         returnStatus = "签收失败";
  //       }
  //       break;
  //     case 10:
  //       {
  //         returnStatus = "退回";
  //       }
  //       break;
  //     default:
  //       {
  //         returnStatus = "未知状态";
  //       }
  //       break;
  //   }
  //   return returnStatus;
  // }
}
