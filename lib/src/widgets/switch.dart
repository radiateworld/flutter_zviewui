import 'package:flutter/material.dart';

/// 开关
/// 支持异步

typedef SwitchChange = Future Function();

class Switch extends StatefulWidget {
  final double? height;
  final double? width;
  final double size;
  final Duration duration;
  final SwitchChange? onChangeBefore;
  final Widget? activeIcon;
  final Widget? unActiveIcon;
  final bool? state;
  const Switch(
      {Key? key,
      this.height,
      this.width,
      required this.size,
      this.duration = const Duration(milliseconds: 800),
      this.onChangeBefore,
      this.activeIcon,
      this.unActiveIcon,
      this.state})
      : super(key: key);

  @override
  _SwitchState createState() => _SwitchState();
}

class _SwitchState extends State<Switch> {
  /// 是否显示异步加载图标
  bool showAyncIcon = false;

  double get left => 0;

  double get right => 0;

  double get top => 0;

  /// 记录switch状态
  late bool state;
  @override
  void initState() {
    state = widget.state ?? false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        bool canNext = true;
        if (widget.onChangeBefore != null) {
          /// 等待接口调用完毕
          canNext = await widget.onChangeBefore!().then((_) {
            return true;
          }).catchError((onError) {
            return false;
          });
        }

        if (canNext) {
          setState(() {
            state = !state;
          });
        }
      },
      child: SizedBox(
        height: widget.height,
        width: widget.width,
        child: Stack(
          children: [
            AnimatedPositioned(
              duration: widget.duration,
              left: left,
              right: right,
              top: top,
              child: Container(
                child: Offstage(
                  offstage: !showAyncIcon,
                  child: const CircularProgressIndicator(),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class test<T> {
  static Future<S?> aa<S>(S value) async {
    S? a = value;
    return a;
  }
}
