/*
 * @Descripttion:  头像组件，带标题跟副标题
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-10-18 15:20:45
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-21 10:32:07
 */

import 'package:flutter/material.dart';
import '../utils/adapter.dart';

import 'media/image_fade_in.dart';

const defaultAvatar = 'assets/images/mine/unloginava.png';

/// 头像组件，带标题跟副标题
class Avatar extends StatelessWidget {
  /// 头像尺寸(有边框要减去边框的宽度)
  final double headSize;

  /// 头像path
  final String? image;

  /// 头像圆角度 默认是圆形
  final double? radius;

  /// 标题
  final Widget? title;

  /// 副标题
  final Widget? subTitle;

  /// title 与subtitle 直接的距离(默认16rpx)
  final double? minVerticalPadding;

  /// 头像跟title之间的距离(默认16rpx)
  final double? horizontalTitleGap;

  final MainAxisAlignment alignment;

  /// 头像图片排版
  final BoxFit fit;

  /// 背景色(默认透明)
  final Color backgroundColor;

  /// 背景内边距
  final EdgeInsetsGeometry backgroundInsets;

  final BoxBorder? avatarBorder;

  final double backgroundHeight;

  final Decoration? decoration;

  final VoidCallback? onTap;

  final List<BoxShadow>? imageBoxShadow;

  /// 头像组件+标题（widget）+副标题（widget）
  ///
  const Avatar(
      {Key? key,
      required this.headSize,
      this.title,
      this.image,
      this.subTitle,
      this.onTap,
      this.horizontalTitleGap,
      this.minVerticalPadding,
      this.imageBoxShadow,
      this.backgroundColor = Colors.transparent,
      this.radius,
      this.fit = BoxFit.cover,
      this.alignment = MainAxisAlignment.start,
      this.backgroundInsets = const EdgeInsets.all(0),
      this.avatarBorder,
      this.backgroundHeight = 0,
      this.decoration})
      : assert((title != null && subTitle != null) || subTitle == null,
            "subTitle不为空，title必传"),
        assert(
            (backgroundColor != Colors.transparent && backgroundHeight != 0) ||
                backgroundColor == Colors.transparent,
            "backgroundColor不是透明需要设置backgroundHeight高度"),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return IntrinsicWidth(
      child: IntrinsicHeight(
        child: GestureDetector(
          onTap: onTap,
          child: Container(
            decoration: decoration ??
                UnderlineTabIndicator(
                    borderSide: BorderSide(
                        width: backgroundHeight, color: backgroundColor),
                    insets: backgroundInsets),
            child: Row(
              mainAxisAlignment: alignment,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                // Container(
                //   width: headSize,
                //   height: headSize,
                //   decoration: BoxDecoration(
                //     border: avatarBorder,
                //     borderRadius: BorderRadius.circular(radius ?? headSize / 2),
                //     boxShadow: imageBoxShadow,
                //     image: decorationImage(path: image, fit: fit),
                //   ),
                // ),

                IntrinsicWidth(
                  child: IntrinsicHeight(
                    child: Container(
                      clipBehavior: Clip.hardEdge,
                      // width: headSize,
                      // height: headSize,
                      decoration: BoxDecoration(
                        border: avatarBorder,
                        boxShadow: imageBoxShadow,
                        borderRadius:
                            BorderRadius.circular(radius ?? headSize / 1.9),
                      ),
                      child: ClipRRect(
                          borderRadius:
                              BorderRadius.circular(radius ?? headSize / 1.9),
                          child: ImageFadeIn.currency(
                            size: Size.square(headSize),
                            image: image ?? defaultAvatar,
                            fit: fit,
                          )
                          // FadeInImage.assetNetwork(
                          //   placeholder: "images/ic_device_image_default.png",
                          //   image: imagePath,
                          //   fit: BoxFit.cover,
                          // ),
                          ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 0,
                  child: Offstage(
                    offstage: title == null,
                    child: Padding(
                      padding:
                          EdgeInsets.only(left: horizontalTitleGap ?? 16.$rpx),
                      child: Flex(
                        direction: Axis.vertical,
                        // mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // 标题
                          Offstage(
                            offstage: title == null,
                            child: title,
                          ),
                          // 副标题
                          Offstage(
                            offstage: subTitle == null,
                            child: Container(
                              margin: EdgeInsetsDirectional.only(
                                  top: minVerticalPadding ?? 16.$rpx),
                              child: subTitle,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
