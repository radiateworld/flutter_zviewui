import 'package:flutter/material.dart';
import 'package:zviewui/src/utils/adapter.dart';

class Picker {
  final List list; // 列表数据 目前单列
  final int initIndex; // 初始下标
  final String confirmText; // 确认文案
  final String cancelText; // 取消文案
  final Function? confirmCallback; // 确认回调
  final Function? cancelCallback; // 取消回调
  final double itemExtent; // 列表高度
  final int showNumber; // 展示数量
  const Picker({
    Key? key,
    required this.list,
    this.initIndex = 0,
    this.confirmText = '确认',
    this.cancelText = '取消',
    this.confirmCallback,
    this.cancelCallback,
    this.itemExtent = 80,
    this.showNumber = 3,
  });

  // 显示弹窗
  Future<T?> showModal<T>(BuildContext context) async {
    return await showModalBottomSheet<T>(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return PickerWidget(
          picker: this,
        );
      },
    );
  }
}

class PickerWidget extends StatefulWidget {
  final Picker picker;
  const PickerWidget({Key? key, required this.picker}) : super(key: key);
  @override
  _PickerWidget createState() => _PickerWidget();
}

class _PickerWidget extends State<PickerWidget> {
  late int selectIndex = 0; // 初始下标
  late bool isAnimate = false; // 是否滚动

  late FixedExtentScrollController _controller = FixedExtentScrollController();

  @override
  void initState() {
    super.initState();
    selectIndex = widget.picker.initIndex;
    _controller =
        FixedExtentScrollController(initialItem: widget.picker.initIndex);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AnimatedPadding(
            padding: MediaQuery.of(context).viewInsets,
            duration: const Duration(milliseconds: 100),
            // 容器
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.zero,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 32.$rpx,
                      vertical: 32.$rpx,
                    ),
                    color: Theme.of(context).scaffoldBackgroundColor,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            Navigator.pop(context);
                            widget.picker
                                .cancelCallback!([_controller.selectedItem]);
                          },
                          child: Text(
                            widget.picker.cancelText,
                            style: TextStyle(
                              fontSize: 30.$rpx,
                              color: Theme.of(context).disabledColor,
                            ),
                          ),
                        ),
                        GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            Navigator.pop(context);
                            widget.picker
                                .confirmCallback!([_controller.selectedItem]);
                          },
                          child: Text(
                            widget.picker.confirmText,
                            style: TextStyle(
                              fontSize: 30.$rpx,
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height:
                        (widget.picker.itemExtent * widget.picker.showNumber)
                            .$rpx,
                    padding: EdgeInsets.symmetric(
                      // horizontal: 32.$rpx,
                      vertical: 32.$rpx,
                    ),
                    child: NotificationListener(
                      onNotification: (ScrollNotification notification) {
                        if (isAnimate) {
                          return false;
                        }
                        if (notification is ScrollStartNotification) {
                          // print("开始滚动");
                        } else if (notification is ScrollUpdateNotification) {
                          // print("正在滚动。。。总滚动距离：${notification.metrics.pixels}");
                        } else if (notification is ScrollEndNotification) {
                          // print("结束滚动");
                          isAnimate = true;
                          Future.delayed(const Duration(milliseconds: 0), () {
                            _controller
                                .animateToItem(_controller.selectedItem,
                                    duration: const Duration(milliseconds: 200),
                                    curve: Curves.easeIn)
                                .then((value) => isAnimate = false);
                          });
                        }
                        return false;
                      },
                      child: ListWheelScrollView.useDelegate(
                        itemExtent: widget.picker.itemExtent.$rpx,
                        perspective: 0.003,
                        diameterRatio: 1.6,
                        controller: _controller,
                        onSelectedItemChanged: (int index) {
                          setState(() {
                            selectIndex = index;
                          });
                          // debugPrint(index.toString());
                          // _controller.animateToItem(index + 1,
                          //     duration: const Duration(milliseconds: 500),
                          //     curve: Curves.easeIn);
                        },
                        childDelegate: ListWheelChildBuilderDelegate(
                          builder: (context, index) {
                            return Column(
                              children: [
                                Container(
                                  width: double.infinity,
                                  height: 1.$rpx,
                                  color: index == selectIndex
                                      ? Theme.of(context).dividerColor
                                      : Colors.transparent,
                                ),
                                Expanded(
                                  child: Container(
                                    width: double.infinity,
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 24.$rpx),
                                    // child: Text('$index'),
                                    child: Text(
                                      Characters(
                                              widget.picker.list[index]['name'])
                                          .join('\u{200B}'),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: index == selectIndex
                                            ? 28.$rpx
                                            : 26.$rpx,
                                        fontWeight: index == selectIndex
                                            ? FontWeight.bold
                                            : FontWeight.normal,
                                        color: index == selectIndex
                                            ? Theme.of(context)
                                                .textTheme
                                                .bodyText1
                                                ?.color
                                            : Theme.of(context)
                                                .unselectedWidgetColor,
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: double.infinity,
                                  height: 1.$rpx,
                                  color: index == selectIndex
                                      ? Theme.of(context).dividerColor
                                      : Colors.transparent,
                                ),
                              ],
                            );
                          },
                          childCount: widget.picker.list.length,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
