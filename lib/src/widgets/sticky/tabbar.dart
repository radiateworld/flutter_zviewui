/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-08 20:13:06
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-09 17:17:05
 */
import 'dart:math';

import '../../utils/adapter.dart';
import 'package:flutter/material.dart';

class StickyTabBar extends SliverPersistentHeaderDelegate {
  final double maxHeight;
  final double minHeight;
  final List list;
  final TabController controller;
  const StickyTabBar({
    required this.maxHeight,
    required this.minHeight,
    required this.list,
    required this.controller,
  }) : super();

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    //创建child子组件
    //shrinkOffset：child偏移值minExtent~maxExtent
    //overlapsContent：SliverPersistentHeader覆盖其他子组件返回true，否则返回false
    //print('shrinkOffset = $shrinkOffset overlapsContent = $overlapsContent');
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      constraints: BoxConstraints(maxHeight: maxHeight, minHeight: minHeight),
      child: TabBar(
          controller: controller,
          indicatorPadding: EdgeInsets.only(bottom: 44.$rpx),
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(
              width: Adapter.rpx(10),
              color: Theme.of(context).primaryColor.withOpacity(0.6),
            ),
          ),
          indicatorSize: TabBarIndicatorSize.label,
          tabs: list
              .map((e) => Tab(
                    text: e,
                  ))
              .toList()),
    );
  }

  //SliverPersistentHeader最大高度
  @override
  double get maxExtent => maxHeight;

  //SliverPersistentHeader最小高度
  @override
  double get minExtent => max(maxHeight, minHeight);

  @override
  bool shouldRebuild(covariant StickyTabBar oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        list != oldDelegate.list;
  }
}
