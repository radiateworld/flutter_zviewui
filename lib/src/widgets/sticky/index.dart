/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-08 17:37:27
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-21 11:25:13
 */
import 'package:flutter/material.dart';

class Sticky extends SliverPersistentHeaderDelegate {
  final Widget child;
  final double minHeight;
  final double maxHeight;
  /// 吸顶
  /// child 高度要固定，不然会报错
  const Sticky({
    required this.child,
    required this.minHeight,
    required this.maxHeight,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return child;
  }

  @override
  double get maxExtent => maxHeight;

  @override

  double get minExtent =>  minHeight;

  @override
  bool shouldRebuild(covariant Sticky oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}
