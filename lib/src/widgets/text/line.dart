/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-29 11:54:08
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-30 21:36:37
 */
import 'package:flutter/material.dart';

/// 多行文本
/// 默认显示两行
/// 超出... 隐藏
class Line extends StatelessWidget {
  final String text;
  final int line;
  final TextOverflow? overflow;

  ///文本样式
  ///
  ///默认
  ///```dart
  ///TextStyle(
  ///  fontSize: 28.$rpx,
  ///  fontWeight: FontWeight.w400,
  ///  color: Config.black,
  ///  height: 36/28
  ///)
  ///```
  final TextStyle? style;

  /// 多行文本
  /// 默认显示两行
  /// 超出... 隐藏
  const Line(
      {Key? key,
      required this.text,
      this.line = 2,
      this.overflow = TextOverflow.ellipsis,
      this.style})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: line,
      overflow: overflow,
      textAlign: TextAlign.left,
      style: Theme.of(context).textTheme.bodyText1?.merge(style),
    );
  }
}
