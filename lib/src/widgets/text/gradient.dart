/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-09 22:17:30
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-10 11:35:21
 */
import 'package:flutter/widgets.dart';

class TextGradient extends StatelessWidget {
  /// 色标数组
  final List<Color> colors;
  /// 要显示的文本
  final String data;
  /// 文本样式
  final TextStyle? style;
  /// 文本渐变
  const TextGradient(
      {Key? key, this.colors = const [], required this.data, this.style})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (Rect bounds) {
        return LinearGradient(
          colors: colors,
          tileMode: TileMode.mirror,
        ).createShader(bounds);
      },
      blendMode: BlendMode.srcATop,
      child: Text(
        data,
        style: style,
      ),
    );
  }
}
/*
另一种实现方式
final Shader linearGradient = LinearGradient(
  colors: <Color>[Color(0xffDA44bb), Color(0xff8921aa)],
).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));
then use it in the TextStyle's foreground

  Text(
        'Hello Gradients!',
        style: new TextStyle(
            fontSize: 60.0,
            fontWeight: FontWeight.bold,
            foreground: Paint()..shader = linearGradient),
      )
*/