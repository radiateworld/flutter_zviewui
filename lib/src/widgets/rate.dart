/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-10-18 14:31:07
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-11 09:52:44
 */
import 'package:flutter/material.dart';
import 'package:zviewui/src/utils/adapter.dart';

/// 评分部件
class Rate extends StatefulWidget {
  final int number;

  /// 评分
  final double score;

  /// 图标
  final IconData? icon;

  /// 图标尺寸
  final double? iconSize;

  final Function(double) change;

  /// 评分部件
  const Rate(
      {Key? key,
      this.number = 5,
      required this.score,
      this.icon,
      this.iconSize,
      required this.change})
      : super(key: key);

  @override
  _RateState createState() => _RateState();
}

class _RateState extends State<Rate> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Row(children: buildWidgetList()),
    );
  }

  List<Widget> buildWidgetList() {
    List<Widget> _list = [];
    for (var i = 0; i < widget.number; i++) {
      double factor = (widget.score - i);
      if (factor >= 1) {
        factor = 1.0;
      } else if (factor < 0) {
        factor = 0;
      }
      _list.add(RateItem(
        factor: factor,
        icon: widget.icon!,
      ));
    }
    return _list.toList();
  }
}

/// 评分单项
class RateItem extends StatelessWidget {
  final double factor;

  /// 图标
  final IconData? icon;

  /// 图标尺寸
  final double? iconSize;
  const RateItem(
      {Key? key, this.factor = 1, this.icon = Icons.star, this.iconSize})
      : super(key: key);

  double get size => iconSize ?? 32.$rpx;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Icon(
          icon,
          color: Theme.of(context).dividerColor,
          size: size,
        ),
        ClipRect(
            child: Align(
          alignment: Alignment.topLeft,
          widthFactor: factor,
          child: Icon(
            icon,
            color: Theme.of(context).primaryColor,
            size: size,
          ),
        ))
      ],
    );
  }
}
