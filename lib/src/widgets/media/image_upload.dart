/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-09-29 22:42:31
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-21 20:57:32
 */
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:intl/intl.dart';
import 'package:sp_util/sp_util.dart';
import 'package:zviewui/src/utils/adapter.dart';

import 'image_fade_in.dart';

/// 是单张图片上传还是多张图片上传
enum UploadType {
  /// 单张图片
  single,

  /// 多张
  multiple
}

/// 上传图片组件
class ImageUpload extends StatefulWidget {
  final double spacing;
  final double runSpacing;

  /// 内边距
  final EdgeInsets? padding;

  /// 最大上传数量
  final int max;

  /// 图片最大尺寸(默认最大2M)
  final int maxSize;

  /// 列表发生改变时的回调
  final void Function(List<String>)? onchange;

  ///
  // final void Function(List)? onfileChange;
  /// 外部传入的列表
  final List<String> imagelist;

  /// 图片尺寸
  final Size size;

  /// 空图片按钮图标大小 默认60%宽度
  final double? iconSize;

  /// 删除图标大小 默认24.rpx
  final double? delIconSize;

  /// 上传类型 single单个上传，multiple多个上传
  final UploadType type;

  /// 属性是设置提交数据的格式，指定将数据回发到服务器时浏览器使用的编码类型。
  /// 默认 multipart/form-data
  /// 以下传值
  /// application/x-www-form-urlencoded：窗体数据被编码为名称/值对。这是标准（默认）的编码格式。
  /// multipart/form-data：窗体数据被编码为一条消息，页上的每个控件对应消息中的一个部分。
  /// text/plain：窗体数据以纯文本形式进行编码，其中不含任何控件或格式字符

  final String enctype;

  /// 接口域名
  final String? action;

  const ImageUpload(
      {Key? key,
      this.padding,
      this.max = 4,
      this.onchange,
      required this.runSpacing,
      this.imagelist = const [],
      required this.size,
      this.type = UploadType.single,
      required this.spacing,
      this.enctype = "multipart/form-data",
      this.action,
      this.iconSize,
      this.delIconSize,
      this.maxSize = 1024 * 1024 * 2})
      : super(key: key);

  @override
  _ImageUploadState createState() => _ImageUploadState();
}

class _ImageUploadState extends State<ImageUpload> {
  /// 图片地址列表
  List<String> list = [];
  List<PickedFile> cacheList = [];
  static final ImagePicker picker = ImagePicker();

  /// 图片尺寸
  late Size size;

  /// 最大图片数量
  late int max;
  @override
  void initState() {
    size = widget.size;
    list = widget.imagelist;
    max = widget.max;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: widget.padding,
      child: Wrap(
        alignment: WrapAlignment.start,
        spacing: widget.spacing,
        runSpacing: widget.runSpacing,
        children: [
          // 遍历展示图片
          ...imageUploadImemList(list),
          // ...imageUploadImemList(cacheList),
          // // 遍历展示缓存图片
          // ...cacheList.map((e) {
          //   return imageUploadImem(image: e.path, type: UploadType.multiple);
          // }).toList(),

          /// 上传按钮
          /// 超出max隐藏
          Offstage(
            offstage: list.length >= widget.max,
            child: imageUploadEmpty(),
          )
        ],
      ),
    );
  }

  Future<PickedFile?> chooseImage() async {
    try {
      return await picker.getImage(
        source: ImageSource.gallery,
        maxWidth: 500,
        maxHeight: 500,
        imageQuality: 100,
      );
    } catch (e) {
      //debugPrint(e.toString() + '报错了！！！');
    }
  }

  /// 上传图片入口
  uploadImg() async {
    //   if (await Permission.) {
    //   // Either the permission was already granted before or the user just granted it.
    // }
    try {
      final PickedFile? pickedFile = await ImagePicker().getImage(
        source: ImageSource.gallery,
        maxWidth: 500,
        maxHeight: 500,
        imageQuality: 100,
      );
      // print(pickedFile!.path);
      // print('4444选择了图片');
      File file = File(pickedFile!.path);
      bool isLarge = await file.length().then((value) {
        return value > widget.maxSize;
      });

      if (isLarge) {
        // return MyToast(S.current.lage);
      }
      // _imageFileList!.add(pickedFile!);
      // //debugPrint(_imageFileList![0].to());
      upload(pickedFile).then((res) {
        // print('3333333311111111133333');
        // print(res);
        // print(jsonDecode(res.data));
        list.add(jsonDecode(res.data)['data']['path']);
        setState(() {
          list = list.toList();
        });
        widget.onchange!(list);
        // widget.change([...fileImgs, ..._imageFileList]);
      }).catchError((onError) {
        //debugPrint(onError.toString());
      });
    } catch (e) {
      //debugPrint(e.toString() + '报错了！！！');
    }
  }

  /// 上传图片
  Future upload(PickedFile image) async {
    String path = image.path;
    String url = widget.action ?? '';
    image.readAsBytes();
    var name = path.substring(path.lastIndexOf("/") + 1, path.length);

    // FormData formData = new FormData.fromMap({
    //   "file": new MultipartFile.fromFile (new File(path), name)
    // });
    // //print(path);
    // //print(name);
    // //print('^^^^^^^^^^^^^^');
    FormData formdata = FormData.fromMap({
      "resourceType": "image",
      "categoryId": "10",
      "purposeType": "avatg",
      "modelType": "app",
      "uploadType": "out",
      "name": name,
      "file": await MultipartFile.fromFile(path, filename: name)
      // "file":  MultipartFile.fromBytes(image.readAsBytes(), filename: name)
    });

    // EasyLoading.show(maskType: EasyLoadingMaskType.clear);

    String? token = SpUtil.getString("token");
    //debugPrint(token);
    /*debugPrint(MultipartFile.fromFile(path, filename: name)
         .then((value) => value)
         .toString());*/
    // //debugPrint(formdata.files.toString());
    // //print('*********************************************上传图片=====');
    // print(url);
    // print('33333333311111111');
    Dio dio = Dio(BaseOptions(baseUrl: url, headers: {
      "Authorization": token,
      "Accept": "application/json",
      "Content-Type": widget.enctype,
      "Lang": Intl.getCurrentLocale(),
      "contentType": widget.enctype
    }));

    //添加拦截器
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) async {
      //debugPrint(
      // "request ${formdata.files}| ${options.data.fields} | ${options.uri}|${options.headers}");

      // //print(token);
      // //print('_____________________________________________________');
      return handler.next(options);
    }, onResponse: (response, handler) {
      return handler.next(response);
    }, onError: (DioError e, handler) {
      //debugPrint('error---------' + e.message);
      return handler.next(e);
    }));

    return dio.post<String>("/upload", data: formdata).then((value) {
      // EasyLoading.dismiss();
      return value;
    }).catchError((onError) {
      // EasyLoading.dismiss();
    });
  }

  /// 删除回调
  onDelete({required int index}) {
    switch (widget.type) {
      case UploadType.single:
        list.removeAt(index);
        setState(() {
          list = list.toList();
          widget.onchange!(list);
        });
        break;
      // 多个图片上传
      case UploadType.multiple:
        cacheList.removeAt(index);
        setState(() {
          cacheList = cacheList.toList();
        });
        break;
      default:
    }
  }

  /// 上传图片占位符
  Widget imageUploadEmpty() {
    return Builder(builder: (BuildContext context) {
      return GestureDetector(
        onTap: () {
          /// 点击上传图片
          //debugPrint('3333333333333333');
          uploadImg();
        },
        child: Container(
          width: size.width,
          height: size.height,
          color: Theme.of(context).dividerColor,
          alignment: Alignment.center,
          child: Icon(
            Icons.photo_camera_rounded,
            size: widget.iconSize ?? size.width * 0.6,
            color: const Color(0xff969698),
          ),
        ),
      );
    });
  }

  /// 图片展示子项
  List<Widget> imageUploadImemList(List list) {
    List<Widget> template = [];

    for (int i = 0; i < list.length; i++) {
      // ImageUploadFile item;
      // if (list[i] is Map<String, dynamic>) {
      //   item = ImageUploadFile.fromJson(list[i] as Map<String, dynamic>);
      // } else {
      //   item = ImageUploadFile.fromJson(list[i] as Map<String, dynamic>);
      // }

      template.add(Stack(
        children: [
          GestureDetector(
              onTap: () {
                /// 点击预览图片
              },
              child: ImageFadeIn.currency(size: size, image: list[i])),
          Positioned(
            top: 10.$rpx,
            right: 10.$rpx,
            child: GestureDetector(
              onTap: () {
                onDelete(index: i);
              },
              child: Icon(
                Icons.remove_circle_outline,
                size: widget.delIconSize ?? 24.$rpx,
                color: Theme.of(context).primaryIconTheme.color,
              ),
            ),
          )
        ],
      ));
    }

    return template.toList();
  }

  /// 底部弹窗
  // void _showSelectionDialog(BuildContext context) {
  //   showModalBottomSheet(
  //     context: context,
  //     isScrollControlled: true,
  //     builder: (ctx) {
  //       return Container(
  //         color: Colors.white,
  //         height: 170,
  //         child: Column(
  //           crossAxisAlignment: CrossAxisAlignment.center,
  //           children: <Widget>[
  //             GestureDetector(
  //               child: _itemCreat(context, '相机'),
  //               onTap: () {
  //                 Navigator.pop(context);
  //                 getImage(ImageSource.camera);
  //               },
  //             ),
  //             GestureDetector(
  //               child: _itemCreat(context, '相册'),
  //               onTap: () {
  //                 Navigator.pop(context);
  //                 getImage(ImageSource.gallery);
  //               },
  //             ),
  //             GestureDetector(
  //               child: Padding(
  //                 padding: EdgeInsets.only(top: 10),
  //                 child: _itemCreat(context, '取消'),
  //               ),
  //               onTap: () {
  //                 Navigator.pop(context);
  //               },
  //             )
  //           ],
  //         ),
  //       );
  //     },
  //   );
  // }
}

class ImageUploadFile {
  final String? id;
  final String image;

  ImageUploadFile({this.id, required this.image});

  factory ImageUploadFile.fromJson(
    Map<String, dynamic> json,
  ) {
    assert(json['path'] != null, 'path是必传的');
    return ImageUploadFile(
      id: json['id'] != null ? json['id'] as String? : null,
      image: json['path'] as String,
    );
  }
}
