/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-12-30 10:00:12
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-30 10:19:31
 */
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';

typedef PageChanged = void Function(int index);

class PhotoPreview extends StatefulWidget {
  final List galleryItems; //图片列表
  final int defaultImage; //默认第几张
  final PageChanged? pageChanged; //切换图片回调
  final Axis direction; //图片查看方向
  final BoxDecoration? decoration; //背景设计

  const PhotoPreview(
      {Key? key,
      required this.galleryItems,
      this.defaultImage = 0,
      this.pageChanged,
      this.direction = Axis.horizontal,
      this.decoration})
      : super(key: key);
  @override
  _PhotoPreviewState createState() => _PhotoPreviewState();
}

class _PhotoPreviewState extends State<PhotoPreview> {
  int tempSelect = 0;
  @override
  void initState() {
    tempSelect = widget.defaultImage + 1;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: [
          Container(
              child: PhotoViewGallery.builder(
                  scrollPhysics: const BouncingScrollPhysics(),
                  builder: (BuildContext context, int index) {
                    return PhotoViewGalleryPageOptions(
                      imageProvider: NetworkImage(widget.galleryItems[index]),
                    );
                  },
                  scrollDirection: widget.direction,
                  itemCount: widget.galleryItems.length,
                  backgroundDecoration:
                      widget.decoration,
                  pageController:
                      PageController(initialPage: widget.defaultImage),
                  onPageChanged: (index) => setState(() {
                        tempSelect = index + 1;
                        if (widget.pageChanged != null) {
                          widget.pageChanged!(index);
                        }
                      }))),
          Positioned(
            ///布局自己换
            right: 20,
            top: 20,
            child: Text(
              '$tempSelect/${widget.galleryItems.length}',
              style: const  TextStyle(color: Colors.black),
            ),
          )
        ],
      ),
    );
  }
}
