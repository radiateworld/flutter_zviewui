/*
 * @Descripttion: 
 * @version: v0.0.1
 * @Author: 黄志勇
 * @Date: 2021-09-10 10:20:00
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-28 18:25:28
 */

import 'package:flutter/material.dart';
import 'package:zviewui/src/utils/is_net_resource.dart';

/// 带图片占位符
class ImageFadeIn extends StatelessWidget {
  /// 图片尺寸
  final Size? size;

  final BoxFit fit;

  ///占位图片（本地文件）
  final AssetImage placeholder;

  /// 要展示的图片
  ///
  /// AssetImage("assets/images/placeholderimg.png")
  ///
  /// NetworkImage("assets/images/placeholderimg.png")
  final ImageProvider image;

  /// 带图片占位符
  const ImageFadeIn(
      {Key? key,
      this.size,
      required this.image,
      this.fit = BoxFit.cover,
      this.placeholder = const AssetImage("assets/images/placeholderimg.png")})
      : super(key: key);

  ///image String  静态，网络通用
  factory ImageFadeIn.currency(
      {required Size? size, required String image, BoxFit fit = BoxFit.cover}) {
    late ImageProvider<Object> imageWidget;
    if (image.isEmpty) {
      imageWidget = const AssetImage("assets/images/placeholderimg.png");
    }
    if (isNetResource(image)) {
      imageWidget = NetworkImage(image);
    } else {
      imageWidget = AssetImage(image);
    }
    // print(size);
    // print('^^^^^^^^^^^');
    // print(fit);
    return ImageFadeIn(size: size, image: imageWidget, fit: fit);
  }

  static const String errorimg = 'assets/images/errorimg.png';

  @override
  Widget build(BuildContext context) {
    double? width;
    double? height;
    // print(size?.width);
    // print('88888888');
    // print(fit);
    switch (fit) {
      case BoxFit.fitWidth:
        width = size?.width ?? double.infinity;
        height = null;
        break;
      case BoxFit.fitHeight:
        // width = double.infinity;
        width = null;
        // height = null;
        break;
      case BoxFit.cover:
        width = size!.width;
        height = size!.height;
        break;
      case BoxFit.contain:
        width = size!.width;
        height = size!.height;
        break;
      default:
        width = null;
        height = null;
    }
    // print(width);
    // print('55555555555555555');
    return FadeInImage(
      width: width,
      height: height,
      fit: fit,
      imageErrorBuilder: (context, error, stackTrace) {
        return Image.asset(
          errorimg,
          fit: fit,
          width: width,
          height: height,
        );
      },
      placeholder: placeholder, //占位图片（本地文件）
      image: image, //网络图片
    );
  }
}
