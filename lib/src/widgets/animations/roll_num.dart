/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-12-30 14:30:19
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-31 09:39:29
 */
import 'package:flutter/material.dart';

typedef RollNumBack = bool Function();
enum RollNumState {
  ///
  start,

  ///暂停
  pause,
  end,
}

// 数字滚动部件
class RollNum extends StatefulWidget {
  final num begin;
  final num end;
  final TextStyle? style;
  final RollNumState state;
  final RollNumBack? onend;

  /// 保留多少位小数
  final int fixed;
  final Duration duration;

  /// 数字滚动部件
  const RollNum({
    Key? key,
    required this.begin,
    required this.end,
    this.style,
    this.duration = const Duration(milliseconds: 2000),
    this.fixed = 2,
    this.state = RollNumState.start,
    this.onend,
  }) : super(key: key);

  @override
  _RollNumState createState() => _RollNumState();
}

class _RollNumState<T> extends State<RollNum>
    with SingleTickerProviderStateMixin {
  late Animation animation;
  late AnimationController controller;
  @override
  void initState() {
    controller = AnimationController(duration: widget.duration, vsync: this);
    // //debugPrint(widget.begin);
    // //debugPrint(widget.end);
    // //debugPrint((widget.begin is int).toString());
    // //debugPrint('*' * 40);
    if (widget.begin is int && widget.end is int) {
      /// Tween 即使传入int 类型 滚动还是double 类型  IntTween 则没有这种情况
      animation = IntTween(begin: widget.begin as int, end: widget.end as int)
          .animate(controller)
        ..addListener(() {
          setState(() {
            // the state that has changed here is the animation object’s value
          });
        });
    } else {
      animation =
          Tween(begin: widget.begin, end: widget.end).animate(controller)
            ..addListener(() {
              setState(() {
                // the state that has changed here is the animation object’s value
              });
            });
    }
    controller.forward();
    super.initState();
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // if (animation.value is int) {
    // //  print('int');
    // }
    // if (animation.value is double) {
    // //  print('double');
    // }
    return Text(
      animation.value is double
          ? double.parse(animation.value.toString())
              .toStringAsFixed(widget.fixed)
          : animation.value.toString(),
      style: widget.style,
    );
  }
}
