/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-24 17:29:54
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-25 16:57:07
 */
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';

/// 上下滚动是提示盒子
class NoticeContainer extends StatefulWidget {
  final Widget Function(BuildContext, int) itemBuilder;
  final int count;
  final double height;

  /// 上下滚动是提示盒子
  const NoticeContainer(
      {Key? key,
      required this.itemBuilder,
      required this.count,
      required this.height})
      : super(key: key);

  @override
  _NoticeContainerState createState() => _NoticeContainerState();
}

class _NoticeContainerState extends State<NoticeContainer> {
  // late PageController controller;
  int currentIndex = 0;
  late Timer timer;
  @override
  void initState() {
    // controller = PageController()..addListener(() {

    // });

    //  timer =  Timer.periodic( const Duration(seconds: 1), (timer){
    //    if(widget.count==1){
    //      timer.cancel();
    //    }
    //     // controller.animateTo(offset, duration: duration, curve: this)
    //     if(currentIndex< widget.count-1){
    //       setState(() {
    //         currentIndex++;
    //       });
    //     }else if(currentIndex==widget.count-1){
    //       setState(() {
    //         currentIndex=0;
    //       });
    //     }
    //     // controller.animateToPage(currentIndex, duration: const  Duration(seconds: 1), curve: Curves.easeIn);
    //   });
    super.initState();
  }

  @override
  void dispose() {
    // timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: SizedBox(
        height: widget.height,
        // color: Config.theme,
        child: Swiper(
          // key: UniqueKey(),
          scrollDirection: Axis.vertical,
          // containerWidth: 600.$rpx,
          // containerHeight: widget.height,
          loop: true,
          autoplay: widget.count != 1,
          itemHeight: widget.height,
          itemCount: widget.count,
          duration: 2000,
          autoplayDelay: 3000,
          onIndexChanged: (int index) {
            setState(() {
              currentIndex = index;
            });
          },
          itemBuilder: (BuildContext context, int index) {
            return AnimatedOpacity(
              opacity: currentIndex == index ? 1 : 0,
              duration: const Duration(milliseconds: 2000),
              child: widget.itemBuilder(context, index),
            );
          },
        ),
      ),
    );
  }
}
