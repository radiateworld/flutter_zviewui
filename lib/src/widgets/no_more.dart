/*
 * @Descripttion: 
 * @version: v0.0.1
 * @Author: 黄志勇
 * @Date: 2021-09-11 15:36:58
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-09 10:09:29
 */

import 'package:flutter/material.dart';
import 'package:zviewui/src/utils/adapter.dart';

class NoMore extends StatelessWidget {
  final String? title;
  const NoMore({
    Key? key,
    this.title,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 750.$rpx,
      padding: EdgeInsets.all(40.$rpx),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: TextStyle(
            fontSize: 22.$rpx,
            fontWeight: FontWeight.w400,
            color: Theme.of(context).primaryTextTheme.bodyText1?.color,
            height: 40 / 22,
          ),
          children: const [
            TextSpan(text: ">>  "),
            TextSpan(
              text: "我是有底线的",
            ),
            TextSpan(text: "  <<")
          ],
        ),
      ),
    );
  }
}
