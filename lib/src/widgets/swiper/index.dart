/*
 * @Descripttion: 再次封装的轮播组件
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-10-18 12:02:52
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-27 18:28:22
 */

import 'package:flutter/material.dart';
import 'package:zviewui/zviewui.dart';

import 'widgets/indicator_number.dart';
import 'widgets/swiper_indicator.dart';

import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import '../media/image_fade_in.dart';

/// 指示器类型
enum SwiperPluginType {
  /// 线条
  line,

  /// 数字
  number,
}

/// 轮播组件再封装
class ZSwiper extends StatelessWidget {
  /// 图片数组
  final List<String> list;

  /// 轮播比例
  final double aspectRatio;

  /// 轮播指示器
  final SwiperPluginType type;

  /// 轮播控制器
  final SwiperController? controller;

  /// 轮播滑动
  final ScrollPhysics? physics;

  /// 轮播点击事件
  final VoidCallback? onTap;
  const ZSwiper(
      {Key? key,
      required this.list,
      this.type = SwiperPluginType.line,
      required this.aspectRatio,
      this.controller,
      this.physics,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (list.isEmpty) {
      return const SizedBox.shrink();
    } else {
      return SizedBox(
        width: double.infinity,
        child: AspectRatio(
          aspectRatio: aspectRatio,
          child: Swiper(
            /// 当只有一张图片时不允许滑动
            physics: list.length > 1
                ? physics ?? const AlwaysScrollableScrollPhysics()
                : const NeverScrollableScrollPhysics(),
            controller: controller,
            autoplay: false,
            itemBuilder: swiperItem,
            duration: 800,
            autoplayDelay: 3000,
            itemCount: list.length,
            scrollDirection: Axis.horizontal,
            pagination: pagination(),
            // pagination:SwiperCustomPagination(
            //     builder:(BuildContext context, SwiperPluginConfig config){
            //         return new YourOwnPaginatipon();
            //     }
            // )
            // control: const SwiperControl(),
          ),
        ),
      );
    }
  }

  /// 指示器
  SwiperPlugin? pagination() {
    if (list.length < 2) {
      return null;
    }
    switch (type) {
      case SwiperPluginType.line:
        return swiperPagination(list);
      case SwiperPluginType.number:
        return swiperPaginationNumber(swiperList: list);
      default:
        break;
    }
  }

  Widget swiperItem(BuildContext context, int index) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return GestureDetector(
          onTap: onTap,

          /// 好奇怪，在image那里判断会报错，升级后看看是还会报错
          child: isNetResource(list[index])
              ? ImageFadeIn(
                  placeholder: const AssetImage("assets/images/mine_plac.png"),
                  size: Size(
                      constraints.biggest.width, constraints.biggest.height),
                  image: NetworkImage(list[index]), //网络图片
                )
              : ImageFadeIn(
                  placeholder: const AssetImage("assets/images/mine_plac.png"),
                  size: Size(
                      constraints.biggest.width, constraints.biggest.height),
                  image: AssetImage(list[index]), //网络图片
                ),
        );
      },
    );
  }
}
