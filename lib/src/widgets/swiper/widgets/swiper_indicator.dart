/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-08-13 18:20:08
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-12 20:13:47
 */

import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:zviewui/src/utils/adapter.dart';

SwiperPlugin? swiperPagination(swiperList) {
  return SwiperPagination(
    alignment: Alignment.bottomCenter,
    builder: SwiperCustomPagination(
        builder: (BuildContext context, SwiperPluginConfig config) {
      return indicator(
          config: config, context: context, swiperList: swiperList);
    }),
  );
}

Widget indicator(
    {required BuildContext context,
    required SwiperPluginConfig config,
    required List swiperList,
    double size = 7}) {
  List<Widget> template = [];
  for (int i = 0; i < swiperList.length; i++) {
    template.add(Container(
      // color:config.activeIndex==i?Colors.white: Colors.white38,
      width: Adapter.rpx(config.activeIndex == i ? size * 4 : size * 2),
      height: Adapter.rpx(size * 2),
      margin:
          EdgeInsets.only(left: Adapter.rpx(size), right: Adapter.rpx(size)),
      decoration: BoxDecoration(
          color: config.activeIndex == i
              ? Theme.of(context).primaryColor
              : Theme.of(context).primaryColor.withOpacity(0.4)),
      // borderRadius: BorderRadius.all(Radius.circular(size * 2))),
      // transform:
      // AnimatedBuilder(
      //   animation: animation,
      //   child: child,
      //   builder: (BuildContext context, Widget child) {
      //     return ;
      //   },
      // ),
    )

        // AnimatedSwitcher(
        //   transitionBuilder: (child, anim) {
        //     return ScaleTransition(
        //       child: child,
        //       scale: anim,
        //     );
        //   },
        //   duration: Duration(milliseconds: 350),
        //   child: IconButton(
        //       key: ValueKey(_icon),
        //       icon: Icon(_icon),
        //       onPressed: () {
        //         setState(() {
        //           if (_icon == Icons.delete) {
        //             _icon = Icons.done;
        //           } else {
        //             _icon = Icons.delete;
        //           }
        //         });
        //       }),
        // )

        );
  }

  return Flex(
    direction: Axis.horizontal,
    mainAxisAlignment: MainAxisAlignment.center,
    children: template.toList(),
  );
}
