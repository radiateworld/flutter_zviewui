/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-10-18 14:05:49
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-10-19 15:04:16
 */

import '../../../utils/adapter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';

SwiperPlugin? swiperPaginationNumber(
    {required List swiperList, Alignment? alignment = Alignment.bottomRight}) {
  return SwiperPagination(
    alignment: alignment,
    builder: SwiperCustomPagination(
        builder: (BuildContext context, SwiperPluginConfig config) {
      return UnconstrainedBox(
        child: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(horizontal: 14.$rpx),
          constraints: BoxConstraints(minWidth: 90.$rpx),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(
                  28.$rpx,
                ),
              ),
              color: const Color.fromRGBO(0, 0, 0, 0.44)),
          child: Text.rich(TextSpan(
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 28.$rpx,
                  fontWeight: FontWeight.w400),
              children: [
                TextSpan(text: (config.activeIndex + 1).toString()),
                const TextSpan(text: '/'),
                TextSpan(text: swiperList.length.toString())
              ])),
        ),
      );
    }),
  );
}
