/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-11 14:45:45
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-27 10:14:15
 */

import 'package:flutter/material.dart';
import 'cut_widget.dart';

import '../utils/adapter.dart';
import '../utils/is_net_resource.dart';
import 'media/image_fade_in.dart';

typedef Transformersfunc = void Function(int index);

/// 金刚区部件
class Transformers extends StatelessWidget {
  /// 子项构造器
  final Widget Function(BuildContext, int)? itemBuilder;

  /// 一行显示多少个（默认4个）
  final int crossAxisCount;

  /// 多少行（默认1个）
  final int verticalAxisCount;

  /// 长宽比
  final double childAspectRatio;

  /// 在不使用itemBuilder下对logo 进行修改
  final Widget Function(int index)? imageBuild;

  /// 主轴间隔(默认垂直方向)
  final double mainAxisSpacing;

  /// 垂直与主轴方向间隔
  final double crossAxisSpacing;

  /// 金刚区所有样式集合
  final TransformersStyle style;

  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? gridPadding;

  /// 要展示的数据
  /// 内部会转换成二维数组
  final List<Map<String, dynamic>> list;

  final double height;

  final Transformersfunc? onTap;

  /// 组件背景色
  final Color backgroundColor;

  final ScrollPhysics? gridViewPhysics;

  final ScrollPhysics? listViewPhysics;

  /// 金刚区组件
  ///

  const Transformers(
      {Key? key,
      this.itemBuilder,
      this.crossAxisCount = 4,
      this.verticalAxisCount = 1,
      required this.childAspectRatio,
      required this.mainAxisSpacing,
      required this.crossAxisSpacing,
      this.imageBuild,
      this.style = const TransformersStyle(),
      required this.list,
      this.padding,
      this.gridPadding,
      this.onTap,
      required this.height,
      this.listViewPhysics,
      this.backgroundColor = Colors.white,
      this.gridViewPhysics = const NeverScrollableScrollPhysics()})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor,
      height: height,
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return ListView.custom(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            physics: listViewPhysics,
            padding: padding ?? EdgeInsets.zero,
            childrenDelegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                // print(crossAxisCount * verticalAxisCount * index);
                // print('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
                // print(crossAxisCount * verticalAxisCount * (index + 1));
                // print('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
                int start = crossAxisCount * verticalAxisCount * index;
                int end = crossAxisCount * verticalAxisCount * (index + 1);
                if (end > list.length) {
                  end = list.length;
                }
                return Container(
                  constraints: constraints,
                  child: GridView.builder(
                      padding: gridPadding ?? EdgeInsets.zero,
                      physics: gridViewPhysics,
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: crossAxisCount,
                        childAspectRatio: childAspectRatio,
                        mainAxisSpacing: mainAxisSpacing,
                        crossAxisSpacing: crossAxisSpacing,
                      ),
                      itemCount: end - start,
                      itemBuilder: (BuildContext context, int gridindex) {
                        return (itemBuilder ?? defaultBuilder)(
                            context, (index + 1) * gridindex);
                        // return Text(gridindex.toString());
                      }),
                );
                // return Text('33333333333333');
                // return Container();
              },
              childCount:
                  (list.length / (verticalAxisCount * crossAxisCount)).ceil(),
            ),
          );
        },
      ),
    );
  }

  /// 默认
  Widget defaultBuilder(BuildContext context, int index) {
    final TransformItemData item = TransformItemData.fromJson(list[index],
        size: style.imageSize ?? Size(68.$rpx, 68.$rpx), fit: style.fit);
    // assert(item.titile！=null&&item.subtitle,'');
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap!(index);
          return;
        }
        //如果设置了路由
        if (item.url != null && item.url!.isNotEmpty && item.url != '#') {
          // print(item.url);
          Uri url = Uri.parse(item.url!);
          // print(url.path);
          // print(url.host);
          // print(url.data);
          // print('object');
          // print(url.origin);
          // print('object');
          // print(url.pathSegments);
          // print(url.queryParameters);
          // print('object');
          if (url.queryParameters.isEmpty) {
            Navigator.pushNamed(
              context,
              url.path,
            );
          } else {
            Navigator.pushNamed(context, url.path,
                arguments: url.queryParameters);
          }
        } else if (item.url == '#') {
          // MyToast(S.current.toBeOpened);
        }
      },
      child: Container(
        color: Colors.transparent,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            buildImage(index, item),
            CutWidget(
              state: item.title != null && item.title!.isNotEmpty,
              correct: Container(
                padding: style.titlePadding!,
                child: Text(
                  item.title ?? '',
                  style: const TextStyle()
                      .merge(Theme.of(context).textTheme.subtitle1)
                      .merge(style.titleStyle),
                ),
              ),
            ),
            CutWidget(
              state: item.subtitle != null && item.subtitle!.isNotEmpty,
              correct: Container(
                padding: style.subTitlePadding,
                child: Text(
                  item.subtitle ?? '',
                  style: const TextStyle()
                      .merge(Theme.of(context).textTheme.subtitle2)
                      .merge(style.subtitleStyle),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  /// 构建Image
  /// 如果logoBuld 不等于空
  ///
  Widget buildImage(index, TransformItemData item) {
    if (imageBuild != null) {
      return imageBuild!(index);
    } else {
      return item.image!;
    }
  }
}

class TransformItemData {
  final String? title;
  final String? subtitle;
  final Widget? image;
  final String? url;
  final Size? imageSize;

  const TransformItemData({
    this.title,
    this.subtitle,
    this.image,
    this.imageSize,
    this.url,
  });

  factory TransformItemData.fromJson(Map<String, dynamic> json,
      {BoxFit fit = BoxFit.cover, Size? size}) {
    assert(json['image'] != null && size != null || json['image'] == null,
        '传入image时size不能为空');
    return TransformItemData(
        title: json['title'] != null ? json['title'] as String? : null,
        subtitle: json['subtitle'] != null ? json['subtitle'] as String? : null,
        url: json['url'] != null ? json['url'] as String? : null,
        image: json['image'] != null
            ? isNetResource(json['image'])
                ? ImageFadeIn(
                    size: size, image: NetworkImage(json['image']), fit: fit)
                : Image.asset(
                    json['image'],
                    fit: fit,
                    width: size?.width,
                    height: size?.height,
                  )
            : null);
  }

  static Image handleImage({required String path, BoxFit? fit, Size? size}) {
    if (path.startsWith('http')) {
      return Image.network(
        path,
        fit: fit,
        width: size!.width,
        height: size.height,
      );
    } else {
      return Image.asset(
        path,
        fit: fit,
        width: size!.width,
        height: size.height,
      );
    }
  }
}

///金刚区样式类
class TransformersStyle {
  ///标题样式
  ///默认
  ///```dart
  ///TextStyle(
  ///  fontSize: 28.$rpx,
  ///  color: Config.black,
  ///  height: 40/28,
  ///  fontWeight: FontWeight.w500
  ///)
  ///```
  final TextStyle? titleStyle;

  ///副标题样式
  ///默认
  ///```dart
  /// TextStyle(
  ///   fontSize: 26.$rpx,
  ///   color: Config.grey,
  ///   height: 40/26,
  ///   fontWeight: FontWeight.w400
  /// )
  ///```
  ///
  final TextStyle? subtitleStyle;

  /// 图片尺寸
  final Size? imageSize;

  /// 副标题内边距
  final EdgeInsetsGeometry? subTitlePadding;

  /// 标题内边距
  final EdgeInsetsGeometry? titlePadding;

  /// 图片排版
  final BoxFit fit;
  const TransformersStyle({
    this.imageSize,
    this.titleStyle,
    this.titlePadding,
    this.subTitlePadding,
    this.subtitleStyle,
    this.fit = BoxFit.cover,
  });
}
