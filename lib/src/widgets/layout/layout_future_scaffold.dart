/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-10-25 15:11:21
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-24 10:04:27
 */

import 'package:flutter/material.dart';
import "../../utils/adapter.dart";

/// 二级页面通用布局
/// 带请求接口Future 布局组件(目前只支持请求一次)
class LayoutFutureScaffold<T> extends StatelessWidget {
  /// 标题部件
  final Widget? appBarTitle;

  final PreferredSizeWidget? appBar;

  /// 标题
  final String? title;

  /// 初始化接口
  final Future<T> future;

  /// 页面背景色
  final Color? backgroundColor;

  /// 页面返回监听
  final Future<bool> Function()? onWillPop;

  /// 是否可以返回上一页，默认为true,可以返回上一页
  final bool canback;

  final bool showNavBar;

  final Widget? leading;

  /// appbar -> actions
  final List<Widget>? actions;

  /// widget 构造方法
  final Widget Function(BuildContext, T data) builder;

  const LayoutFutureScaffold(
      {Key? key,
      this.appBar,
      this.title,
      required this.future,
      required this.builder,
      this.appBarTitle,
      this.backgroundColor,
      this.onWillPop,
      this.canback = true,
      this.showNavBar = true,
      this.leading,
      this.actions})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop ??
          () async {
            /// 返回上一页刷新
            Navigator.pop(context, true);
            return false;
          },
      child: GestureDetector(
        onTap: () {
          /// 点击空白处失去焦点
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Scaffold(
          backgroundColor: backgroundColor,
          appBar: showNavBar == true
              ? appBar ??
                  AppBar(
                    // 如果
                    leading: canback ? leading : const SizedBox.shrink(),
                    title: appBarTitle ?? (title == null ? null : Text(title!)),

                    actions: actions,
                  )
              : const PreferredSize(
                  preferredSize: Size.zero,
                  child: SizedBox.shrink(),
                ),
          body: SafeArea(
            child: FutureBuilder<T>(
              future: future,
              builder: (BuildContext context, AsyncSnapshot<T> snapshot) {
                // 请求已结束
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    // 请求失败，显示错误
                    // return Text("Error: ${snapshot.error}");
                    return const SizedBox.shrink();
                  } else {
                    // 请求成功，显示数据
                    return builder(context, snapshot.data!);
                  }
                } else {
                  // 请求未结束，显示loading
                  // return const Center(
                  //   child: CircularProgressIndicator(),
                  // );
                  return Center(
                    child: Image.asset('assets/images/loading.gif',
                        width: 160.$rpx, height: 160.$rpx),
                  );
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
