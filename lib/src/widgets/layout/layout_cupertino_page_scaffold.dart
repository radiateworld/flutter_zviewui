import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// 左滑返回上一页
class CupertionScrold extends StatelessWidget {
  const CupertionScrold({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Center(
        child: CupertinoButton(
          child: const Text(
            "跳转详情页",
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
          color: CupertinoColors.destructiveRed,
          onPressed: () {
            Navigator.of(context)
                .push(CupertinoPageRoute(builder: (BuildContext context) {
              return Container();
            }));
          },
        ),
      ),
    );
  }
}
