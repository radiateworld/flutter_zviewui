/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-10-11 09:56:56
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-25 09:41:34
 */

import 'package:flutter/material.dart';
import '../sticky/index.dart';

import 'layout_scaffold.dart';

class LayoutScrollView extends StatelessWidget {
  /// 头部部件
  final Widget? header;

  /// 底部部件
  final Widget? footer;

  /// 仅支持sliver前缀部件
  final List<Widget> slivers;

  /// appbar
  final SliverAppBar? sliverAppBar;

  /// 标题部件
  final Widget? appBarTitle;

  /// 标题
  final String? title;

  /// 头部导航栏
  final AppBar? appBar;
  final Color? footerBg;
  final Future? future;
  final bool loading;

  /// 吸顶固定最大高度
  final double? stickyMaxHeight;

  /// 吸顶固定最小高度
  final double? stickyMinHeight;

  final ScrollController? controller;

  final ScrollPhysics? physics;
  final bool showNavBar;
  const LayoutScrollView(
      {Key? key,
      this.header,
      this.footer,
      required this.slivers,
      this.sliverAppBar,
      this.footerBg,
      this.appBarTitle,
      this.title,
      this.loading = true,
      this.appBar,
      this.future,
      this.stickyMaxHeight,
      this.stickyMinHeight,
      this.controller,
      this.physics,
      this.showNavBar = true})
      : super(key: key);

  double get maxHeight => stickyMaxHeight ?? stickyMinHeight ?? 0;
  double get minHeight => stickyMinHeight ?? 0;
  @override
  Widget build(BuildContext context) {
    return loading == false
        ? const SizedBox.shrink()
        : LayoutScaffold(
            showNavBar: showNavBar,
            title: title,
            appBarTitle: appBarTitle,
            appBar: appBar,
            footer: footer,
            body: CustomScrollView(
              physics: physics,
              controller: controller,
              slivers: [
                // SliverAppBar 部件
                // SliverOffstage(
                //   offstage: sliverAppBar == null,
                //   sliver: sliverAppBar),
                /// 固定在顶部
                SliverSafeArea(
                  top: true,
                  sliver: SliverOffstage(
                    offstage: header == null,
                    sliver: SliverPersistentHeader(
                      floating: false,
                      pinned: true,
                      delegate: Sticky(
                        maxHeight: maxHeight,
                        minHeight: minHeight,
                        child: header ?? const SizedBox.shrink(),
                      ),
                    ),
                  ),
                ),
                ...slivers,
              ],
            ),
          );
  }
}
