/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-10-11 10:41:32
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-27 09:22:41
 */
import 'package:flutter/material.dart';

/// 二级页面通用布局
/// 带导航栏
/// 固定头部
/// 固定底部
class LayoutScaffold extends StatelessWidget {
  /// 标题部件
  final Widget? appBarTitle;

  /// 标题
  final String? title;

  /// 头部导航栏
  final AppBar? appBar;

  /// 固定的头部
  final Widget? header;

  /// 固定的底部
  final Widget? footer;

  /// 页面主体
  final Widget body;

  /// 页面背景色
  final Color? backgroundColor;

  /// 页面返回监听
  final Future<bool> Function()? onWillPop;

  /// 是否可以返回上一页，默认为true,可以返回上一页
  final bool canback;

  /// 是否显示navbar
  final bool showNavBar;

  /// footer 背景色默认是白色
  final Color? footerBg;

  ///
  final Widget? leading;

  /// navbar 右边区域
  final List<Widget>? actions;
  const LayoutScaffold({
    Key? key,
    this.appBar,
    this.title,
    this.appBarTitle,
    this.header,
    this.footer,
    required this.body,
    this.backgroundColor,
    this.footerBg,
    this.onWillPop,
    this.canback = true,
    this.showNavBar = true,
    this.leading,
    this.actions,
  })  : assert(
            !(title != null && appBarTitle != null), 'title跟appBarTitle不必同时设置'),
        assert(
            (appBar != null && title == null && appBarTitle == null) ||
                (appBar == null),
            '设置了appBar后，title，跟appBarTitle无效'),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop ??
          () async {
            /// 返回上一页刷新
            if (Navigator.canPop(context)) {
              Navigator.pop(context, true);
            }
            return false;
          },
      child: GestureDetector(
        onTap: () {
          /// 点击空白处失去焦点
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Scaffold(
          backgroundColor: backgroundColor,
          appBar: showNavBar
              ? appBar ??
                  AppBar(
                    // backwardsCompatibility: true,
                    // iconTheme: IconThemeData(size: 120.$rpx, color: Config.red),
                    // 如果
                    leading: canback ? leading : const SizedBox.shrink(),
                    title: appBarTitle ?? (title == null ? null : Text(title!)),

                    actions: actions,
                  )
              : const PreferredSize(
                  preferredSize: Size.zero,
                  child: SizedBox.shrink(),
                ),

          /// 底部
          bottomNavigationBar: Offstage(
            offstage: footer == null,
            child: Container(
              color: footerBg,
              width: double.infinity,
              child: IntrinsicHeight(
                child: SafeArea(
                  child: footer ?? const SizedBox.shrink(),
                ),
              ),
            ),
          ),
          body: Container(
            padding: EdgeInsets.zero,
            width: double.infinity,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ///固定的头部
                Offstage(
                  offstage: header == null,
                  child: SafeArea(
                    child: SizedBox(
                      width: double.infinity,
                      child: header ?? const SizedBox.shrink(),
                    ),
                  ),
                ),
                // 主体
                Expanded(
                  flex: 1,
                  child: SizedBox(width: double.infinity, child: body),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
