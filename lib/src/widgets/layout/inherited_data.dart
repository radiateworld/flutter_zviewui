/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-08 15:20:34
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-08 17:08:39
 */
import 'package:flutter/material.dart';

/// 数据继承
class InheritedData extends InheritedWidget {
  final dynamic data;

  const InheritedData({
    Key? key,
    required this.data,
    required Widget child,
  }) : super(key: key, child: child);

  static InheritedData? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<InheritedData>();
  }

  @override
  bool updateShouldNotify(InheritedData old) => true;
}

/* 以下是使用例子 */

class TestInheritedDataWidget extends StatelessWidget {
  const TestInheritedDataWidget({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        InheritedData.of(context)!.data,
        style: const TextStyle(fontSize: 18, color: Colors.pink),
      ),
    );
  }
}

class ParentWidget extends StatefulWidget {
  const ParentWidget({Key? key}) : super(key: key);
  @override
  _ParentWidgetState createState() => _ParentWidgetState();
}

class _ParentWidgetState extends State<ParentWidget> {
  var data = "you are beautiful";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        child: Container(
          color: Colors.cyan,
          width: double.infinity,
          child: InheritedData(
            data: data,
            child: const TestInheritedDataWidget(),
          ),
        ),
        onTap: _buttonClicked,
      ),
    );
  }

  _buttonClicked() {
    // setState(() {
    //   data = "in white";
    // });
  }
}
