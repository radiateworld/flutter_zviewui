import 'package:flutter/material.dart';


/// 购物车页面布局
/// 底部固定 全选+ 总价
///
class LayoutCart extends StatefulWidget {
  final Color? backgroundColor;
  const LayoutCart({Key? key, this.backgroundColor}) : super(key: key);

  @override
  _LayoutCartState createState() => _LayoutCartState();
}

class _LayoutCartState extends State<LayoutCart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
        body: Container());
  }
}
