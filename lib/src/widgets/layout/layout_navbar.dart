/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-10-29 17:30:03
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-27 20:37:06
 */

import 'package:flutter/material.dart';

import '../scroll_view/scroll_view.dart';
import 'layout_scaffold.dart';

/// 带顶部页签（静态数据）
class LayoutNavbar<T> extends StatefulWidget {
  /// 标题部件
  final Widget? appBarTitle;

  /// 标题
  final String? title;

  /// 头部导航栏
  final AppBar? appBar;

  /// 页面背景色
  final Color? backgroundColor;

  /// 页面返回监听
  final Future<bool> Function()? onWillPop;

  /// 是否可以返回上一页，默认为true,可以返回上一页
  final bool canback;

  /// 是否显示navbar
  final bool showNavBar;

  /// footer 背景色默认是白色
  final Color? footerBg;

  ///
  final Widget? leading;

  /// navbar 右边区域
  final List<Widget>? actions;

  /// navbar 部件
  final Widget? navBar;

  final List<T> tabs;

  final Widget Function(BuildContext context, int index)? pageBuilder;

  final List<Widget> pages;

  final ScrollPhysics? physics;
  final ScrollPhysics? tabBarViewphysics;

  const LayoutNavbar(
      {Key? key,
      this.appBarTitle,
      this.title,
      this.appBar,
      this.backgroundColor,
      this.onWillPop,
      this.canback = true,
      this.showNavBar = true,
      this.footerBg,
      this.leading,
      this.actions,
      this.navBar,
      required this.tabs,
      this.pageBuilder,
      this.pages = const [],
      this.physics,
      this.tabBarViewphysics})
      : super(key: key);

  @override
  State<LayoutNavbar<T>> createState() => _LayoutNavbarState<T>();
}

class _LayoutNavbarState<T> extends State<LayoutNavbar<T>> {
  bool hasmore = true;

  @override
  Widget build(BuildContext context) {
    return LayoutScaffold(
      appBar: widget.appBar,
      header: widget.navBar,
      body: builTabBarView(),
    );
  }

  TabBarView builTabBarView() {
    List<Widget> tabPage = widget.pages;
    if (tabPage.isEmpty && widget.pageBuilder != null) {
      for (int i = 0; i < widget.tabs.length; i++) {
        tabPage.add(ZScrollView(
          physics: widget.physics,
          count: widget.tabs.length,
          hasmore: hasmore,
          itemBuilder: widget.pageBuilder,
        ));
      }
    }
    return TabBarView(
      physics: widget.tabBarViewphysics,
      children: tabPage,
    );
  }
}
