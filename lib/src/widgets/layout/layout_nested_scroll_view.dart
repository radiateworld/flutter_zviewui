/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-16 09:24:17
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-22 11:59:49
 */

import 'package:flutter/material.dart';

import '../cut_widget.dart';

class LayoutHeadBody extends StatelessWidget {
  /// 标题部件
  final Widget? appBarTitle;

  /// 标题
  final String? title;

  /// 头部导航栏
  final AppBar? appBar;

  /// 固定的底部
  final Widget? footer;

  /// 页面主体
  final Widget body;

  /// 常用于tabbar
  final Widget? bodyhead;

  /// 页面背景色
  final Color? backgroundColor;

  /// 页面返回监听
  final Future<bool> Function()? onWillPop;

  /// 是否可以返回上一页，默认为true,可以返回上一页
  final bool canback;

  final bool showNavBar;

  final Color? footerBg;

  final Widget? leading;

  /// 头部构建
  ///```dart
  /// (BuildContext context, bool innerBoxIsScrolled) {
  ///   return [
  ///      SliverAppBar(
  ///         automaticallyImplyLeading: false,
  ///         expandedHeight: 660.$rpx,
  ///         flexibleSpace: FlexibleSpaceBar(
  ///         collapseMode: CollapseMode.none,
  ///         background: SizedBox(
  ///           height: Adapter.rpx(660),
  ///           child: ZSwiper(
  ///             aspectRatio: 750 / 660,
  ///                 list: bannerList,
  ///               )),
  ///              ),
  ///           )
  ///          ];
  /// },
  /// ```
  final List<Widget> Function(BuildContext, bool)? header;

  const LayoutHeadBody(
      {Key? key,
      this.appBarTitle,
      this.title,
      this.appBar,
      this.header,
      this.footer,
      required this.body,
      this.onWillPop,
      this.backgroundColor,
      this.footerBg,
      this.canback = true,
      this.showNavBar = false,
      this.leading,
      this.bodyhead})
      : assert(
            !(title != null && appBarTitle != null), 'title跟appBarTitle不必同时设置'),
        assert(
            (appBar != null && title == null && appBarTitle == null) ||
                (appBar == null),
            '设置了appBar后，title，跟appBarTitle无效'),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop ??
          () async {
            /// 返回上一页刷新
            if (Navigator.canPop(context)) {
              Navigator.pop(context, true);
            }
            return false;
          },
      child: GestureDetector(
        onTap: () {
          /// 点击空白处失去焦点
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Scaffold(
            backgroundColor: backgroundColor,
            appBar: showNavBar == true
                ? appBar ??
                    AppBar(
                      // 如果
                      leading: canback ? leading : const SizedBox.shrink(),
                      title:
                          appBarTitle ?? (title == null ? null : Text(title!)),
                    )
                : const PreferredSize(
                    preferredSize: Size.zero,
                    child: SizedBox.shrink(),
                  ),

            /// 底部
            bottomNavigationBar: Offstage(
              offstage: footer == null,
              child: Container(
                color: footerBg ??
                    Theme.of(context).bottomNavigationBarTheme.backgroundColor,
                width: double.infinity,
                child: IntrinsicHeight(
                  child: SafeArea(
                    child: footer ?? const SizedBox.shrink(),
                  ),
                ),
              ),
            ),
            body: NestedScrollView(
              headerSliverBuilder: header ??
                  (BuildContext context, bool innerBoxIsScrolled) {
                    return [];
                  },
              body: Column(
                children: [
                  CutWidget(state: bodyhead != null, correct: bodyhead!),
                  Expanded(
                    child: body,
                  ),
                  // tabs导航
                  // Container(
                  //   height: 98.$rpx,
                  //   decoration: const BoxDecoration(
                  //       color: Config.white,
                  //       border: Border(
                  //           bottom: BorderSide(
                  //         color: Config.pageBg,
                  //       ))),
                  //   child: TabBar(
                  //     tabs: [
                  //       S.current.commodity,
                  //       // S.current.details,
                  //       S.current.comment
                  //     ].build((int index, dynamic item) {
                  //       return Tab(
                  //         child: Text(
                  //           item,
                  //           softWrap: false,
                  //         ),
                  //       );
                  //     }),
                  //     controller: _tabController,
                  //     //TabBar指针的宽度
                  //     indicatorWeight: Adapter.rpx(6),
                  //     indicatorColor: Config.theme,
                  //     //设置指针的内边距使其横向缩短
                  //     // indicatorPadding: EdgeInsets.symmetric(
                  //     //   horizontal: Adapter.rpx(24),
                  //     // ),
                  //     //选中后的style
                  //     labelStyle: TextStyle(
                  //       fontSize: Adapter.rpx(32),
                  //       fontWeight: FontWeight.bold,
                  //     ),
                  //     //未选中的style
                  //     unselectedLabelStyle: TextStyle(
                  //       fontSize: Adapter.rpx(24),
                  //     ),
                  //     //选中后的颜色
                  //     labelColor: Config.black,
                  //     unselectedLabelColor: Config.black,
                  //     indicatorPadding: EdgeInsets.only(bottom: 18.$rpx),
                  //   ),
                  // ),

                  // // 详情内容
                  // Expanded(
                  //   child: body?? TabBarView(
                  //     controller: _tabController,
                  //     children: [
                  //       // 规格
                  //       Commodity(
                  //         addressInfo: addressInfo,
                  //         evaluationList: evaluationList.isEmpty
                  //             ? []
                  //             : evaluationList.sublist(
                  //                 0,
                  //                 evaluationList.length <= 3
                  //                     ? evaluationList.length
                  //                     : 3),
                  //         controller: _tabController,
                  //         detailInfo: detailInfo,
                  //         serviceList: serviceList,
                  //       ),

                  //       Evaluation(
                  //         reload: () {
                  //           getDetail();
                  //         },
                  //         productUuid: detailInfo!.main.productUuid,
                  //       ),
                  //     ],
                  //   ),
                  // ),
                ],
              ),
            )),
      ),
    );
  }
}
