/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-04 11:53:35
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-21 10:38:44
 */
import 'package:flutter/material.dart';
import 'package:zviewui/src/utils/adapter.dart';

class LayoutTabs extends StatefulWidget {
  final int index;
  final List<Map> pageList;
  const LayoutTabs({Key? key, this.index = 0, required this.pageList})
      : super(key: key);

  @override
  State<LayoutTabs> createState() => _LayoutTabsState();
}

class _LayoutTabsState extends State<LayoutTabs> {
  late PageController pageController;
  int _currentIndex = 0;
  @override
  void initState() {
    pageController = PageController(keepPage: true)
      ..addListener(() {
        setState(() {
          _currentIndex = pageController.page!.toInt();
        });
      });
    _currentIndex = widget.index;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: pageController,
        children: widget.pageList.map<Widget>((e) => e["page"]).toList(),
      ),
      bottomNavigationBar: Container(
        constraints: BoxConstraints(minHeight: 98.$rpx),
        decoration: BoxDecoration(
          color: Theme.of(context).bottomNavigationBarTheme.backgroundColor,
          boxShadow: const [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.1),
              offset: Offset(0, -2),
              blurRadius: 4,
              spreadRadius: 1,
            ),
          ],
        ),
        child: SafeArea(
          child: BottomNavigationBar(
            backgroundColor:
                Theme.of(context).bottomNavigationBarTheme.backgroundColor,
            elevation: 0,
            currentIndex: _currentIndex, //配置对应的索引值选中
            onTap: (int index) {
              /// 跳转到指定页面
              pageController.animateTo(index.toDouble(),
                  curve: Curves.ease,
                  duration: const Duration(milliseconds: 300));
              // setState(() {
              //改变状态
              // _currentIndex = index;
              // pageList[index]['count'] = '';
              // 切换需要优化
              // if (_currentIndex == 3) {
              //   SystemUiOverlayStyle systemUiOverlayStyle =
              //       const SystemUiOverlayStyle(
              //     statusBarColor: Config.theme,
              //     statusBarIconBrightness: Brightness.light,
              //   );
              //   SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
              // } else {
              //   SystemUiOverlayStyle systemUiOverlayStyle =
              //       const SystemUiOverlayStyle(
              //     statusBarColor: Config.white,
              //     statusBarIconBrightness: Brightness.dark,
              //   );
              //   SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
              // }
              // });
            },
            // backgroundColor: Config.tabsBarBg,
            selectedFontSize: 0,
            unselectedFontSize: 0,
            iconSize: 36.0, //icon的大小r
            // selectedItemColor: Colors.white,
            unselectedItemColor: Theme.of(context)
                .bottomNavigationBarTheme
                .unselectedItemColor, //未选中字体颜色
            fixedColor: Theme.of(context)
                .bottomNavigationBarTheme
                .selectedItemColor, //选中的颜色
            type: BottomNavigationBarType.fixed, //配置底部tabs可以有多个按钮
            items: tabsIconList(widget.pageList),
          ),
        ),
      ),
    );
  }
}

// 返回底部导航栏
List<BottomNavigationBarItem> tabsIconList(tablist) {
  List<BottomNavigationBarItem> tabIonList = [];
  iconImage(int index, [bool isactive = false]) => Stack(
        children: <Widget>[
          Image.asset(
            "assets/images/tabs/${tablist[index]['icon']}${isactive ? '_sel' : ''}.png",
            fit: BoxFit.cover,
            width: 30.00,
            height: 30.00,
          ),
          tablist[index]["count"] != null &&
                  tablist[index]["count"] != '' &&
                  int.parse(tablist[index]["count"]) > 0
              ? Positioned(
                  right: 0,
                  child: Container(
                    padding: const EdgeInsets.all(1),
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(Adapter.rpx(12)),
                    ),
                    constraints: BoxConstraints(
                      minWidth: Adapter.rpx(24),
                      minHeight: Adapter.rpx(24),
                    ),
                    child: Text(
                      '${tablist[index]["count"]}', //通知数量
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: Adapter.rpx(16),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              : const SizedBox.shrink()
        ],
      );

  for (var i = 0; i < tablist.length; i++) {
    tabIonList.add(
      BottomNavigationBarItem(
        icon: iconImage(i),
        activeIcon: iconImage(i, true),
        // label: tablist[i]['name'],
        label: '',
        // tooltip: '1'
      ),
    );
  }
  return tabIonList.toList();
}
