/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-12-01 16:33:35
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-01 22:08:15
 */
import 'dart:async';
import '../../utils/timer_format.dart';
import 'package:flutter/material.dart';

/// 倒计时组件
class CountdownTime extends StatefulWidget {
  /// 倒计时结束回调
  final VoidCallback end;

  /// 数据更新，默认1秒
  final Duration duration;

  /// 倒计时时间差，毫秒数精确到秒，
  final int interval;

  /// 构建等待文本
  final Widget Function(String)? buildWaiting;

  /// 构建运行中文本
  final Widget Function(String) buildRuning;

  /// 构建结束文本
  final Widget Function(String)? buildEnd;

  /// 步长 更新一次timeNum 加多少
  final int step;

  /// 倒计时时间格式化
  /// 'yyyy-MM-dd HH:mm:ss' 默认显示秒
  final String format;

  /// 倒计时组件
  const CountdownTime(
      {Key? key,
      required this.end,
      required this.interval,
      this.buildWaiting,
      required this.buildRuning,
      this.buildEnd,
      this.duration = const Duration(seconds: 1),
      this.step = 1,
      this.format = 'ss'})
      : super(key: key);

  @override
  _CountdownTimeState createState() => _CountdownTimeState();
}

class _CountdownTimeState extends State<CountdownTime> {
  final StreamController<int> _streamController = StreamController<int>();

  /// 记录倒计时次数
  int timeNum = 0;

  /// 记录初始interval，如果跟widget.interval不相同时需要清除定时器，重新开始
  int initInterval = 0;
  late Timer? _timer;
  late int stop;
  @override
  void initState() {
    initInterval = widget.interval;
    startTimer();
    super.initState();
  }

  void startTimer() {
    _timer = Timer.periodic(widget.duration, (timer) {
      if (initInterval != widget.interval) {
        setState(() {
          initInterval = widget.interval;
          timeNum = 0;
          _streamController.close();
        });
      } else {
        timeNum += widget.step;
      }

      /******************  判断是否结束倒计时  *****************/
      if (widget.interval - timeNum < 0) {
        timer.cancel();
        widget.end();
        _streamController.close();
      } else {
        _streamController.sink.add(widget.interval - timeNum);
      }
    });
  }

  /// 取消倒计时的计时器。
  void _cancelTimer() {
    // 计时器（`Timer`）组件的取消（`cancel`）方法，取消计时器。
    _timer?.cancel();
  }

  @override
  void dispose() {
    // 关掉不需要的Stream;
    _timer?.cancel();
    _streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
      stream: _streamController.stream, //
      // initialData: 10,// a Stream<int> or null
      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
        if (snapshot.hasError) return Text('Error: ${snapshot.error}');
        // print(snapshot.data);
        // print(widget.format);

        /// 要展示的值
        String shwoTxt = TimerFormat.formatData(
            snapshot.data ?? (widget.interval > 0 ? widget.interval : 0),
            widget.format);

        switch (snapshot.connectionState) {
          case ConnectionState.none:
            // return Text('没有Stream');
            return const SizedBox.shrink();
          case ConnectionState.waiting:
            return (widget.buildWaiting ?? widget.buildRuning)(shwoTxt);
          case ConnectionState.active:
            return widget.buildRuning(shwoTxt);
          case ConnectionState.done:
            _cancelTimer();
            return (widget.buildEnd ?? widget.buildRuning)(shwoTxt);
        }
      },
    );
  }
}
