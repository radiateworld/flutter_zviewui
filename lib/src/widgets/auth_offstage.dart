import 'package:flutter/material.dart';
import 'package:sp_util/sp_util.dart';

/// 没有登陆，直接隐藏部件
class AuthOffstage extends StatelessWidget {
  /// AuthOffstage子部件
  final Widget child;
  const AuthOffstage({ Key? key,required this.child }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    String? token = SpUtil.getString('token');
    return Offstage(offstage: token==null||token=='',child: child,);
  }
}
