/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-10-11 15:03:33
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-24 10:19:45
 */
import 'package:flutter/widgets.dart';
import 'package:flutter_html/flutter_html.dart';
export 'package:flutter_html/flutter_html.dart';
class ZHtml extends StatelessWidget {
  final String? data;
  final Map<String, Style>? style;

  ///
  ///```dart
  /// //demo
  ///Html(
  ///  data: title??S.current.titleHtml,
  ///  tagsList: Html.tags..addAll(["login-type"]),
  ///  customRender: {
  ///   "login-type": (RenderContext context, Widget child) {
  ///      return Text(loginTitle,
  ///          style: TextStyle(
  ///            color: Config.theme,
  ///            fontSize: context.style.fontSize!.size,
  ///         ));
  ///    }
  ///  },
  ///  style: {
  ///    'body': Style(
  ///        backgroundColor: Colors.transparent,
  ///        padding: EdgeInsets.zero,
  ///        margin: EdgeInsets.zero),
  ///    "p": Style(
  ///       margin: EdgeInsets.zero,
  ///        padding: EdgeInsets.zero,
  ///       textAlign: TextAlign.left,
  ///       fontSize: FontSize(38.$rpx),
  ///       fontWeight: FontWeight.bold,
  ///       letterSpacing: 2,
  ///       lineHeight: const LineHeight(26 / 19),
  ///        color: Config.black)
  ///  },
  /// )
  ///```
  const ZHtml({Key? key, required this.data, this.style}) : super(key: key);

  /// 默认样式
  static final defaultStyle = {
    'body': Style(
      padding: const EdgeInsets.all(0),
      margin: const EdgeInsets.all(0),
    ),
    "img": Style(
      width: double.infinity,
    ),
    "video": Style(

        // width: double.infinity,
        ),
    'p': Style(
      padding: const EdgeInsets.all(0),
      margin: const EdgeInsets.all(0),
    ),
  };
  @override
  Widget build(BuildContext context) {
    // 合并样式
    defaultStyle.addAll(style ?? {});
    return Html(data: data, style: defaultStyle);
  }
}

///dart

///
