/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-12-27 10:05:24
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-27 10:11:55
 */
import 'package:flutter/material.dart';

class Code extends StatelessWidget {
  final Widget child;
  const Code({ Key? key,required this.child, }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  AnimatedContainer(
    duration: Duration(seconds: 1),
    
    onEnd:(){},
    child: child,);
  }
}