/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-09 11:52:45
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-01 11:32:50
 */

import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../empty.dart';
import '../../utils/adapter.dart';

import '../cut_widget.dart';
import '../no_more.dart';

class Waterfall extends StatefulWidget {
  /// 滚动控制器
  final ScrollController? controller;

  /// 瀑布流数据
  final List data;

  /// 子部件构造器
  final Widget Function(BuildContext, int) itemBuilder;

  /// 控制当前项的排版
  // final StaggeredTile? Function(int)? staggeredTileBuilder;

  /// 是否隐藏 空状态跟底线 默认是false
  final bool hideEmpty;

  /// 左右边距
  final double? crossAxisSpacing;

  /// 上下边距
  final double? mainAxisSpacing;

  /// 多少列
  final int? crossAxisCount;

  final EdgeInsetsGeometry? padding;

  /// true 锁住不请求接口
  final bool lock;

  /// 下拉刷新回调
  final Future<void> Function()? onRefresh;

  /// 滑动到底部触发的上拉加载更多回调
  final Future<void> Function(int)? scrolltolower;

  /// 是否隐藏空状态跟底线，默认是false
  final bool hideEnd;

  /// 距离底部多少距离触发上拉加载更多回调
  final double lowerThreshold;

  /// 是否手动显示空状态
  final bool hasmore;

  final ScrollPhysics? physics;

  /// 瀑布流组件
  ///
  const Waterfall(
      {Key? key,
      this.padding,
      this.hasmore = true,
      this.controller,
      required this.data,
      this.hideEmpty = false,
      required this.itemBuilder,
      // this.staggeredTileBuilder,
      this.crossAxisSpacing,
      this.mainAxisSpacing,
      this.crossAxisCount,
      this.onRefresh,
      this.lock = false,
      this.hideEnd = false,
      this.scrolltolower,
      this.lowerThreshold = 0,
      this.physics})
      : super(key: key);

  @override
  State<Waterfall> createState() => _WaterfallState();
}

class _WaterfallState extends State<Waterfall> {
  static bool isLoading = false;
  late ScrollController scrollController;
  int page = 1;
  @override
  void initState() {
    scrollController = ScrollController();
    scrollController.addListener(() {
      ///判断当前滑动位置是否到底，触发加载更多回调
      // //print('当前滑动距离${_scrollController.position.pixels}');
      // //print('最大滑动距离${_scrollController.position.maxScrollExtent}');
      // //print('目标滑动距离${_scrollController.position.maxScrollExtent-50.0}');
      // //print(widget.canLoadMore);
      // //print('000000000000000000000000000000000');
      if (scrollController.position.pixels >=
          scrollController.position.maxScrollExtent - widget.lowerThreshold) {
        // //print('4442222222222222222');
        // //print(isLoading);
        // //print(widget.canLoadMore);
        if (!widget.lock && !isLoading && hasmore) {
          // //print('上拉加载更多');
          // setState(() {
          isLoading = true;
          // });
          if (widget.scrolltolower != null) {
            page++;
            widget.scrolltolower!(page).then((res) {
              // Future.delayed(const Duration(microseconds: 400),(){

              // setState(() {
              isLoading = false;
              // });
              // });
            });
          }
        } else if (!hasmore) {}
      } else {}
    });
    super.initState();
  }

  bool get hasmore => widget.hideEnd || widget.hasmore;

  Future onRefresh() async {
    page = 1;
    return widget.onRefresh!();
  }

  Widget universal({required Widget child}) {
    if (widget.onRefresh == null) {
      return child;
    } else {
      return RefreshIndicator(
        onRefresh: widget.onRefresh!,
        child: child,
      );
    }
  }

  // @override
  // Widget build(BuildContext context) {
  //   return universal(
  //     child: ListView(
  //       padding: widget.padding,
  //       // controller: widget.controller,
  //       // physics: widget.physics,
  //       shrinkWrap: true,
  //       children: [
  //         // StaggeredGridView.custom(gridDelegate: gridDelegate, childrenDelegate: childrenDelegate)
  //         StaggeredGridView.countBuilder(
  //           itemCount: widget.data.length,
  //           shrinkWrap: true,
  //           crossAxisCount: widget.crossAxisCount ?? 2,
  //           crossAxisSpacing: widget.crossAxisSpacing ?? 20.$rpx,
  //           mainAxisSpacing: widget.mainAxisSpacing ?? 20.$rpx,
  //           itemBuilder: widget.itemBuilder,
  //           staggeredTileBuilder: widget.staggeredTileBuilder ??
  //               (index) => const StaggeredTile.fit(1),
  //         ),
  //         Offstage(
  //           offstage: widget.hideEmpty,
  //           child: CutWidget(
  //               state: widget.data.isNotEmpty,
  //               correct: const NoMore(),
  //               fault: const EmptyList()),
  //         )
  //       ],
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return universal(
      child: CustomScrollView(
        shrinkWrap: true,
        controller: scrollController,
        physics: widget.physics,
        // physics: NeverScrollableScrollPhysics(),
        slivers: <Widget>[
          // SliverGrid.custom(crossAxisCount: widget.data.length),

          SliverToBoxAdapter(
            child: GridView.custom(
                gridDelegate: SliverStairedGridDelegate(
                  crossAxisSpacing: widget.crossAxisSpacing ?? 20.$rpx,
                  mainAxisSpacing: widget.mainAxisSpacing ?? 20.$rpx,
                  startCrossAxisDirectionReversed: true,
                  pattern: const [
                    StairedGridTile(0.5, 1),
                    StairedGridTile(0.5, 3 / 4),
                    StairedGridTile(1.0, 10 / 4),
                  ],
                ),
                childrenDelegate: SliverChildBuilderDelegate(
                  widget.itemBuilder,
                )),
            // StaggeredGridView.countBuilder(
            //   // physics: widget.physics,
            //   physics: const NeverScrollableScrollPhysics(),
            //   // physics: AlwaysScrollableScrollPhysics(),
            //   shrinkWrap: true,
            //   // controller: scrollController,
            //   padding: widget.padding,
            //   crossAxisCount: widget.crossAxisCount ?? 2,
            //   crossAxisSpacing: widget.crossAxisSpacing ?? 20.$rpx,
            //   mainAxisSpacing: widget.mainAxisSpacing ?? 20.$rpx,
            //   itemCount: widget.data.length,
            //   itemBuilder: widget.itemBuilder,
            //   staggeredTileBuilder: widget.staggeredTileBuilder ??
            //       (index) => const StaggeredTile.fit(1),
            // ),
          ),
          SliverOffstage(
            offstage: hasmore,
            sliver: SliverSafeArea(
              sliver: SliverToBoxAdapter(
                child: CutWidget(
                    state: widget.data.isNotEmpty,
                    correct: const NoMore(),
                    fault: const Empty()),
              ),
            ),
          )
        ],
      ),
    );
  }
}
