/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-10-11 21:25:16
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-24 15:47:09
 */

import 'package:flutter/material.dart';
import 'package:zviewui/src/utils/adapter.dart';

import '../empty.dart';
import '../no_more.dart';

class ZScrollView extends StatefulWidget {
  /// 是否手动显示空状态
  final bool hasmore;

  /// 滑动到底部触发的上拉加载更多回调
  final Future<void> Function(int)? scrolltolower;

  /// 下拉刷新回调
  final Future<void> Function()? onRefresh;

  /// 遍历构建自组件
  final Widget Function(BuildContext, int)? itemBuilder;

  /// 部件列表
  final List<Widget>? children;

  /// 接口数据
  final List? list;

  /// 距离底部多少距离触发上拉加载更多回调
  final double lowerThreshold;

  /// 当接口数据list的长度跟实际不符合时 使用
  /// 可以控制初始加载状态
  final int? count;

  /// 顶部内边距
  final double paddingTop;

  /// 底部内边距
  final double paddingBottom;

  /// true 锁住不请求接口
  final bool lock;

  /// slivers部件数组
  final List<Widget> slivers;

  /// 是否隐藏空状态跟底线，默认是false
  final bool hideEnd;

  /// 滚动部件
  final ScrollPhysics? physics;

  final ScrollController? controller;
  const ZScrollView(
      {Key? key,
      this.hasmore = true,
      this.onRefresh,
      this.scrolltolower,
      this.children = const [],
      this.list = const [],
      this.physics,
      this.lowerThreshold = 0,
      this.count,
      this.itemBuilder,
      this.lock = false,
      this.paddingTop = 0,
      this.slivers = const [],
      this.paddingBottom = 0,
      this.hideEnd = false,
      this.controller})
      : assert(
            (children != null && list != null) ||
                (children != null && list != null) ||
                (children != null && list != null),
            'children和list不能同时存在'),
        assert(children != null || (list != null && itemBuilder != null),
            'itemBuilder和list必须同时存在'),
        assert(children != null || (children != null && itemBuilder == null),
            '传入children时itemBuilder不必传'),
        super(key: key);

  @override
  _ZScrollViewState createState() => _ZScrollViewState();
}

class _ZScrollViewState extends State<ZScrollView> {
  /// 接口是否进行中
  static bool isLoading = false;

  int page = 1;

  late ScrollController scrollController;

  @override
  void initState() {
    // ////debugPrint(widget.children.length);
    // ////debugPrint(widget.list.length);
    scrollController = widget.controller ?? ScrollController();
    // //debugPrint('**********************************************');
    scrollController.addListener(() {
      ///判断当前滑动位置是否到底，触发加载更多回调
      // //debugPrint('当前滑动距离${scrollController.position.pixels}');
      // //debugPrint('最大滑动距离${scrollController.position.maxScrollExtent}');
      // //debugPrint('目标滑动距离${scrollController.position.maxScrollExtent - 50.0}');
      // ////debugPrint(widget.canLoadMore);
      // ////debugPrint('000000000000000000000000000000000');
      if (scrollController.position.pixels >=
          scrollController.position.maxScrollExtent - widget.lowerThreshold) {
        // //debugPrint('4442222222222222222');
        //debugPrint(isLoading.toString());
        ////debugPrint(widget.canLoadMore);
        if (!widget.lock && !isLoading && hasmore) {
          // //debugPrint('2222222222222333333333333333333333333333333333333333');

          // ////debugPrint('上拉加载更多');

          if (widget.scrolltolower != null) {
            if (!isLoading) {
              isLoading = true;
            }
            // //debugPrint('333333333333333333333333333333333333333');
            page++;
            widget.scrolltolower!(page).then((res) {
              // Future.delayed(const Duration(microseconds: 400),(){
              if (isLoading) {
                isLoading = false;
              }

              // });
            });
          } else {
            if (isLoading) {
              isLoading = false;
            }
          }
        } else if (!hasmore) {}
      } else {}
    });

    super.initState();
  }

  bool get hasmore => widget.hideEnd || widget.hasmore;

  Future onRefresh() async {
    page = 1;
    return widget.onRefresh!();
  }

  @override
  void dispose() {
    isLoading = false;
    //debugPrint('退出-----------------------');
    page = 1;
    ////debugPrint('999999999999999999999999999999');

    // if(scrollController.hasListeners){
    scrollController.dispose();
    // }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    /// 判断是需要下拉刷新
    if (widget.onRefresh != null) {
      return RefreshIndicator(
        onRefresh: widget.onRefresh!,
        child: scrollViewBody(),
      );
    } else {
      return scrollViewBody();
    }
  }

  /// 返回列表数量
  int count() {
    if (widget.list!.isNotEmpty) {
      return widget.list!.length;
    } else if (widget.count != null) {
      return widget.count!;
    } else if (widget.children!.isNotEmpty) {
      return widget.children!.length;
    } else {
      return 0;
    }
  }

  /// 滚动主体
  Widget scrollViewBody() {
    return CustomScrollView(
      physics: widget.physics,
      controller: scrollController,
      shrinkWrap: true,
      slivers: <Widget>[
        SliverToBoxAdapter(
          child: SizedBox(
            height: widget.paddingTop,
          ),
        ),
        // Sliverlist build
        widget.itemBuilder != null
            ? SliverList(
                delegate: SliverChildBuilderDelegate(
                  widget.itemBuilder!,
                  childCount: widget.itemBuilder == null ? 0 : count(),
                ),
              )
            : const SliverToBoxAdapter(),
        // sliverlist children
        SliverList(delegate: SliverChildListDelegate(widget.children!)),
        // slivers childen

        ...widget.slivers,

        // 加载中 count + children 或者 count
        SliverOffstage(
          offstage: (isLoading && count() != 0) || count() != 0 || !hasmore,
          sliver: SliverFillViewport(
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
              //创建列表项
              // return const Center(
              //   child: CircularProgressIndicator(),
              // );
              return Center(
                child: Image.asset('assets/images/loading.gif',
                    width: 160.$rpx, height: 160.$rpx),
              );
            }, childCount: 1),
            viewportFraction: 1.0, //占屏幕的比例
          ),
        ),

        // 暂无数据
        SliverOffstage(
          offstage: hasmore || count() != 0,
          sliver: const SliverSafeArea(
            bottom: true,
            sliver: SliverToBoxAdapter(
              child: Empty(),
            ),
          ),
        ),
        // 没有更多数据了
        SliverOffstage(
          offstage: hasmore || count() == 0,
          sliver: const SliverSafeArea(
            bottom: true,
            sliver: SliverToBoxAdapter(
              child: NoMore(),
            ),
          ),
        ),
        SliverToBoxAdapter(
          child: SizedBox(
            height: widget.paddingBottom,
          ),
        ),
      ],
    );
  }
}
