/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-10-11 21:25:16
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-24 15:47:09
 */

import 'package:flutter/material.dart';
import '../empty.dart';
import 'package:zviewui/src/utils/adapter.dart';

import '../no_more.dart';

class ZScrollViewNotification extends StatefulWidget {
  /// 是否手动显示空状态
  final bool hasmore;

  /// 滑动到底部触发的上拉加载更多回调
  final Future<void> Function(int)? scrolltolower;

  /// 下拉刷新回调
  final Future<void> Function()? onRefresh;

  /// 遍历构建自组件
  final Widget Function(BuildContext, int)? itemBuilder;

  /// 部件列表
  final List<Widget>? children;

  /// 接口数据
  final List? list;

  /// 距离底部多少距离触发上拉加载更多回调
  final double lowerThreshold;

  /// 当接口数据list的长度跟实际不符合时 使用
  /// 可以控制初始加载状态
  final int? count;

  /// 顶部内边距
  final double paddingTop;

  /// 底部内边距
  final double paddingBottom;

  /// true 锁住不请求接口
  final bool lock;

  /// slivers部件数组
  final List<Widget> slivers;

  /// 是否隐藏空状态跟底线，默认是false
  final bool hideEnd;

  /// 滚动部件
  final ScrollPhysics? physics;

  final ScrollController? controller;
  const ZScrollViewNotification(
      {Key? key,
      this.hasmore = true,
      this.onRefresh,
      this.scrolltolower,
      this.children = const [],
      this.list = const [],
      this.physics,
      this.lowerThreshold = 0,
      this.count,
      this.itemBuilder,
      this.lock = false,
      this.paddingTop = 0,
      this.slivers = const [],
      this.paddingBottom = 0,
      this.hideEnd = false,
      this.controller})
      : assert(
            (children != null && list != null) ||
                (children != null && list != null) ||
                (children != null && list != null),
            'children和list不能同时存在'),
        assert(children != null || (list != null && itemBuilder != null),
            'itemBuilder和list必须同时存在'),
        assert(children != null || (children != null && itemBuilder == null),
            '传入children时itemBuilder不必传'),
        super(key: key);

  @override
  _ZScrollViewNotificationState createState() =>
      _ZScrollViewNotificationState();
}

class _ZScrollViewNotificationState extends State<ZScrollViewNotification> {
  /// 接口是否进行中
  static bool isLoading = false;

  int page = 1;

  // late ScrollController scrollController;

  @override
  void initState() {
    // //print(widget.children.length);
    // //print(widget.list.length);
    // scrollController= widget.controller?? ScrollController();
    // scrollController.addListener(() {
    //     ///判断当前滑动位置是否到底，触发加载更多回调
    //     // print('当前滑动距离${scrollController.position.pixels}');
    //     // print('最大滑动距离${scrollController.position.maxScrollExtent}');
    //     // print('目标滑动距离${scrollController.position.maxScrollExtent-50.0}');
    //     // //print(widget.canLoadMore);
    //     // //print('000000000000000000000000000000000');
    //     if (scrollController.position.pixels >=
    //         scrollController.position.maxScrollExtent - widget.lowerThreshold) {
    //       // //print('4442222222222222222');
    //       // //print(isLoading);
    //       // //print(widget.canLoadMore);
    //       if (!widget.lock && !isLoading && hasmore) {
    //         if(!isLoading){
    //           setState(() {
    //             isLoading = true;
    //           });
    //         }
    //         // //print('上拉加载更多');

    //         if (widget.scrolltolower != null) {
    //           page++;
    //           widget.scrolltolower!(page).then((res) {
    //             // Future.delayed(const Duration(microseconds: 400),(){
    //             if(isLoading){
    //               setState(() {
    //               isLoading = false;
    //             });
    //             }

    //             // });
    //           });
    //         }
    //       } else if (!hasmore) {}
    //     } else {}
    //   });

    super.initState();
  }

  bool get hasmore => widget.hideEnd || widget.hasmore;

  Future onRefresh() async {
    page = 1;
    return widget.onRefresh!();
  }

  @override
  void dispose() {
    page = 1;
    //print('999999999999999999999999999999');

    // if(scrollController.hasListeners){
    // scrollController.dispose();
    // }
    super.dispose();
  }

  _onScrollNotification(ScrollNotification scrollInfo) {
    // print("pixels=>" + scrollInfo.metrics.pixels.toString());
    // print("minScrollExtent=>" + scrollInfo.metrics.minScrollExtent.toString());
    // print("maxScrollExtent=>" + scrollInfo.metrics.maxScrollExtent.toString());
    ///判断当前滑动位置是否到底，触发加载更多回调
    // //print(widget.canLoadMore);
    // //print('000000000000000000000000000000000');
    if (scrollInfo.metrics.pixels >=
        scrollInfo.metrics.maxScrollExtent - widget.lowerThreshold) {
      // //print('4442222222222222222');
      // //print(isLoading);
      // //print(widget.canLoadMore);
      if (!widget.lock && !isLoading && hasmore) {
        // //print('上拉加载更多');
        // setState(() {
        isLoading = true;
        // });
        if (widget.scrolltolower != null) {
          page++;
          widget.scrolltolower!(page).then((res) {
            // Future.delayed(const Duration(microseconds: 400),(){

            // setState(() {
            isLoading = false;
            // });
            // });
          });
        }
      } else if (!hasmore) {}
    } else {}
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollNotification>(
      child: universal(
        child: CustomScrollView(
          physics: widget.physics,
          // controller: scrollController,
          shrinkWrap: true,
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: SizedBox(
                height: widget.paddingTop,
              ),
            ),
            // Sliverlist build
            widget.itemBuilder != null
                ? SliverList(
                    delegate: SliverChildBuilderDelegate(
                      widget.itemBuilder!,
                      childCount: widget.itemBuilder == null ? 0 : count(),
                    ),
                  )
                : const SliverToBoxAdapter(),
            // sliverlist children
            SliverList(delegate: SliverChildListDelegate(widget.children!)),
            // slivers childen

            ...widget.slivers,

            // 加载中 count + children 或者 count
            SliverOffstage(
              offstage: (isLoading && count() != 0) || count() != 0 || !hasmore,
              sliver: SliverFillViewport(
                delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  //创建列表项
                  // return const Center(
                  //   child: CircularProgressIndicator(),
                  // );
                  Center(
                    child: Image.asset('assets/images/loading.gif',
                        width: 160.$rpx, height: 160.$rpx),
                  );
                }, childCount: 1),
                viewportFraction: 1.0, //占屏幕的比例
              ),
            ),

            // 暂无数据
            SliverOffstage(
              offstage: hasmore || count() != 0,
              sliver: const SliverSafeArea(
                bottom: true,
                sliver: SliverToBoxAdapter(
                  child: Empty(),
                ),
              ),
            ),
            // 没有更多数据了
            SliverOffstage(
              offstage: hasmore || count() == 0,
              sliver: const SliverSafeArea(
                bottom: true,
                sliver: SliverToBoxAdapter(
                  child: NoMore(),
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: widget.paddingBottom,
              ),
            ),
          ],
        ),
      ),
      onNotification: (ScrollNotification scrollInfo) =>
          _onScrollNotification(scrollInfo),
    );
  }

  /// 返回列表数量
  int count() {
    if (widget.list!.isNotEmpty) {
      return widget.list!.length;
    } else if (widget.count != null) {
      return widget.count!;
    } else if (widget.children!.isNotEmpty) {
      return widget.children!.length;
    } else {
      return 0;
    }
  }

  Widget universal({required Widget child}) {
    if (widget.onRefresh == null) {
      return child;
    } else {
      return RefreshIndicator(
        onRefresh: widget.onRefresh!,
        child: child,
      );
    }
  }
}
