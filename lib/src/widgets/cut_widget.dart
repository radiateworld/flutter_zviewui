/*
 * @Descripttion: 
 * @version: v0.0.1
 * @Author: 黄志勇
 * @Date: 2021-08-21 21:16:10
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-09 18:06:31
 */
import 'package:flutter/material.dart';

class CutWidget extends StatelessWidget {
  /// 判断条件（默认是true）
  final bool state;

  ///正确的元素
  // @Deprecated(
  //   '该属性即将废弃，请使用child',
  // )
  final Widget correct;
  final Widget? child;

  /// 错误的元素(默认是SizedBox.shrink())
  final Widget fault;

  /// 通过状态切换元素Offstage有缓存的问题
  ///
  /// state 为ture 显示correct ,默认为true
  ///
  /// state 为false 显示fault , 默认值为SizedBox.shrink()
  /// 不使用Offstage 的原因是 隐藏的元素还是会执行, 类似 v-show
  const CutWidget({
    Key? key,
    this.state = true,
    required this.correct,
    this.child,
    this.fault = const SizedBox.shrink(),
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return state ? child ?? correct : fault;
  }
}
