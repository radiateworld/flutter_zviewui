/*
 * @Descripttion: 
 * @version: v0.0.1
 * @Author: 黄志勇
 * @Date: 2021-09-11 16:29:27
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-21 20:56:48
 */

import 'package:flutter/widgets.dart';
import '../utils/adapter.dart';

/// 空状态部件
class Empty extends StatelessWidget {
  ///空状态部件
  const Empty({Key? key, this.image, this.text = "暂无数据", this.textStyle})
      : super(key: key);
  final Image? image;
  final String text;
  final TextStyle? textStyle;
  static final size = Size(200.$rpx, 100.$rpx);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        image ??
            Image.asset(
              "",
            ),
        Text(
          text,
          style: const TextStyle().merge(textStyle),
        )
      ],
    );
  }
}
