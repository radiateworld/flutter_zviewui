/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-01 14:23:12
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-21 11:24:54
 */

import 'package:flutter/material.dart';
import 'package:zviewui/src/utils/adapter.dart';

/// 底线部件
class EndLine extends StatelessWidget {
  /// 提示的文本
  final String? text;

  /// 线与文本分割的距离
  final double? cubicle;

  /// 左右两边线的尺寸
  final Size? lineSize;

  /// 底线部件
  const EndLine({Key? key, this.text, this.cubicle, this.lineSize})
      : super(key: key);

  double get _cubicle => cubicle ?? 60.$rpx;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 60.$rpx, bottom: 30.$rpx),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: lineSize!.width,
            height: lineSize!.height,
            padding: EdgeInsets.only(left: _cubicle),
            child: const Divider(),
          ),
          Text(text ?? '我是有底线的'),
          Container(
            width: lineSize!.width,
            height: lineSize!.height,
            padding: EdgeInsets.only(right: _cubicle),
            child: const Divider(),
          ),
        ],
      ),
    );
  }
}
