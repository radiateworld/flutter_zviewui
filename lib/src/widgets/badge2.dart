/*
 * @Descripttion: 角标
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-01 11:43:56
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-02 14:58:19
 */

import 'package:flutter/material.dart';
import '../utils/adapter.dart';

/// 角标
class Badge extends StatelessWidget {
  /// 角标子项
  final Widget child;

  /// 角标尺寸
  final Size size;

  /// 角标类型
  final BadgeType type;

  /// 角标位置
  final BadgePosition positionType;

  /// 角标背景色
  final Color? color;

  ///
  final String? text;
  final TextStyle? textStyle;

  final double? offsetRight;
  final double? offsetLeft;
  final double? offsetTop;
  final double? offsetBottom;
  const Badge(
      {Key? key,
      required this.child,
      required this.size,
      this.type = BadgeType.number,
      this.positionType = BadgePosition.topRight,
      this.color,
      this.text,
      this.offsetRight,
      this.offsetLeft,
      this.offsetTop,
      this.offsetBottom,
      this.textStyle})
      : super(key: key);

  static double? right;
  static double? left;
  static double? bottom;
  static double? top;

  BuildContext get context => curentContext ?? context;
  static BuildContext? curentContext;
  @override
  Widget build(BuildContext context) {
    curentContext = context;
    setPosition();

    return OverflowBox(
      child: Stack(
        children: [
          Positioned(
              right: offsetRight ?? right,
              left: offsetLeft ?? left,
              bottom: offsetTop ?? bottom,
              top: offsetBottom ?? top,
              child: badegItem),
          child,
        ],
      ),
    );
  }

  void setPosition() {
    switch (positionType) {
      case BadgePosition.topRight:
        right = -size.width / 2;
        top = -size.height / 2;
        break;
      default:
    }
  }

  Widget get badegItem {
    switch (type) {
      // 点
      case BadgeType.spot:
        return Container(
          color: color ?? Theme.of(context).primaryColor,
          constraints:
              BoxConstraints(maxHeight: size.height, maxWidth: size.width),
        );
      // 数字
      case BadgeType.number:
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 12.$rpx),
          color: color ?? Theme.of(context).primaryColor,
          child: Text(
            text!,
            style: textStyle ??
                TextStyle(
                  fontSize: 24.$rpx,
                  color: Colors.white,
                ),
          ),
          constraints:
              BoxConstraints(maxHeight: size.height, maxWidth: size.width),
        );
      default:
        return const SizedBox.shrink();
    }
  }
}

/// 角标类型
enum BadgeType {
  /// 点
  spot,

  /// 数字
  number,
}

/// 角标定位
enum BadgePosition {
  topLeft,
  topCenter,
  topRight,
  centerLeft,
  centerRight,
  bottomLeft,
  bottomCenter,
  bottomRight,
}
