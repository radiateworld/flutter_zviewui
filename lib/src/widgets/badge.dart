import 'package:flutter/material.dart';

enum BadgeShape { circle, square, spot }

enum BadgePosition { topRight, topLeft, bottomRight, bottomLeft }

const double _kSquareRadius = 1;
const double _kSpotRadius = 4;
const double _kBadgeSize = 18;
const double _kDefaultRadius = -1;

/// 角标组件
class Badge extends StatefulWidget {
  const Badge(
      {Key? key,
      this.color = Colors.red,
      this.shape = BadgeShape.circle,
      this.textStyle = const TextStyle(color: Colors.white, fontSize: 8),
      this.position = BadgePosition.topRight,
      this.hidden = false,
      this.radius = _kDefaultRadius,
      this.text,
      required this.child})
      : super(key: key);

  final Color color;
  final BadgeShape shape;
  final TextStyle textStyle;
  final BadgePosition position;
  final bool hidden;

  /// Each shape will have a default radius.
  /// If u set this value, the [shape] property will be meaningless.
  final double radius;
  final String? text;
  final Widget child;

  @override
  State<Badge> createState() => BadgeState();
}

class BadgeState extends State<Badge> {
  BorderRadius _borderRadius(BadgeShape shape) {
    double radius = 0;

    switch (widget.shape) {
      case BadgeShape.square:
        radius = _kSquareRadius;
        break;
      case BadgeShape.circle:
        radius = _kBadgeSize / 2;
        break;
      case BadgeShape.spot:
        radius = _kSpotRadius;
        break;
      default:
        radius = widget.radius;
    }

    return BorderRadius.circular(radius);
  }

  @override
  Widget build(BuildContext context) {
    BadgeShape shape = widget.text != null ? widget.shape : BadgeShape.spot;
    double size = shape == BadgeShape.spot ? 2 * _kSpotRadius : _kBadgeSize;
    Widget? textChild = widget.text == null
        ? const SizedBox.shrink()
        : Center(
            child: Text(
              widget.text!,
              style: widget.textStyle,
              textAlign: TextAlign.center,
            ),
          );

    double? left, right, top, bottom;
    double offset = size / 3;
    Alignment alignment;
    if (widget.position == BadgePosition.topRight) {
      right = -offset;
      top = -offset;
      alignment = Alignment.topRight;
    } else if (widget.position == BadgePosition.topLeft) {
      left = -offset;
      top = -offset;
      alignment = Alignment.topLeft;
    } else if (widget.position == BadgePosition.bottomRight) {
      bottom = -offset;
      right = -offset;
      alignment = Alignment.bottomRight;
    } else {
      // bottom left
      left = -offset;
      bottom = -offset;
      alignment = Alignment.bottomLeft;
    }

    Widget badge = Positioned(
        left: left,
        top: top,
        bottom: bottom,
        right: right,
        width: size,
        height: size,
        child: Container(
            decoration: BoxDecoration(
                color: widget.color, borderRadius: _borderRadius(shape)),
            child: textChild));

    List<Widget> children =
        widget.hidden ? [widget.child] : [widget.child, badge];

    return Stack(
        clipBehavior: Clip.none, alignment: alignment, children: children);
  }
}
