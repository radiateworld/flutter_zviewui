/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-17 18:04:25
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-27 21:16:42
 */

import 'package:flutter/material.dart';
import 'package:zviewui/src/utils/adapter.dart';
import 'cut_widget.dart';

class LinkList extends StatelessWidget {
  /// 数据
  /// ```json
  /// {
  ///  "logo":"logo图片",
  ///  "title":"必传，标题",
  ///  "subTitle":"副标题",
  ///  "moreText":"靠右文本",
  ///  "toNext": "是否显示向右箭头，默认是不显示",
  ///  "url":"跳转链接",
  ///  "onTap": "点击事件",
  ///  "nextIcon": "是否显示向右箭头，默认是不显示",
  /// }
  /// ```
  final List<Map<String, dynamic>> data;

  /// 样式总集合
  final LinkListStyle style;

  ///列表项高度
  final double? itemHeight;

  /// 子项构造器
  final Widget Function(BuildContext, int)? itemBuilder;
  final ScrollPhysics? physics;

  /// 子项内边距默认左右32.rpx
  final EdgeInsetsGeometry? itemPadding;

  /// 设置traling 回调
  final Widget Function(BuildContext, int)? buildTrailing;
  final EdgeInsetsGeometry? margin;

  /// 列表项装饰器
  final Decoration? Function(BuildContext, int)? buildDecoration;

  /// 列表项背景色
  final Color? itemBgColor;

  /// logo处自定义部件
  final Widget Function(BuildContext, int)? leading;

  /// 右边文本图标点击
  final Function(int)? onTrailingTap;

  /// 显示全部向右图标
  /// 当全部有向右箭头是可以设置为true
  ///
  /// 默认 false
  final bool showAllNext;

  /// 列表统一点击事件
  final Function(int)? onAllTap;

  final TextStyle? Function(int)? unusualTitleStyle;

  /// 链接列表
  const LinkList(
      {Key? key,
      required this.data,
      this.itemBuilder,
      this.style = const LinkListStyle(),
      this.physics = const NeverScrollableScrollPhysics(),
      this.itemHeight,
      this.itemPadding,
      this.buildTrailing,
      this.margin,
      this.itemBgColor,
      this.buildDecoration,
      this.leading,
      this.showAllNext = false,
      this.onTrailingTap,
      this.unusualTitleStyle,
      this.onAllTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.custom(
      padding: const EdgeInsets.all(0),
      physics: physics,
      itemExtent: itemHeight,
      shrinkWrap: true,
      childrenDelegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            final item = LinkItem.fromJson(data[index]);

            return GestureDetector(
              //   /// 点击事件
              onTap: () {
                // 如果设置了点击事件
                if (onAllTap != null) {
                  onAllTap!(index);
                  return;
                }
                if (item.onTap != null) {
                  item.onTap!();
                  return;
                }
                //如果设置了路由
                if (item.url != null &&
                    item.url!.isNotEmpty &&
                    item.url != '#') {
                  ////debugPrint(item.url);
                  Uri url = Uri.parse(item.url!);
                  ////debugPrint(url.path);
                  ////debugPrint(url.host);
                  ////debugPrint(url.data);
                  ////debugPrint('object');
                  ////debugPrint(url.origin);
                  ////debugPrint('object');
                  ////debugPrint(url.pathSegments);
                  ////debugPrint(url.queryParameters);
                  ////debugPrint('object');
                  if (url.queryParameters.isEmpty) {
                    Navigator.pushNamed(
                      context,
                      url.path,
                    );
                  } else {
                    Navigator.pushNamed(context, url.path,
                        arguments: url.queryParameters);
                  }
                } else if (item.url == '#') {
                  // MyToast(S.current.toBeOpened);
                }
              },
              child: Container(
                  alignment: Alignment.centerLeft,
                  padding: itemPadding ??
                      EdgeInsets.symmetric(
                        horizontal: 32.$rpx,
                      ),
                  margin: margin,
                  decoration: (buildDecoration ??
                      (BuildContext context, int index) {
                        if (index < data.length - 1) {
                          return BoxDecoration(
                              color: itemBgColor ?? Theme.of(context).cardColor,
                              border: Border(
                                  bottom: BorderSide(
                                      width: 1.$rpx,
                                      color: Theme.of(context).dividerColor)));
                        }
                        return BoxDecoration(
                          color: itemBgColor,
                        );
                      })(context, index),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      // logo
                      imageBuild(context, index, item.logo,
                          size: style.logoSize),
                      // 标题
                      Expanded(
                        flex: 1,
                        child: Container(
                          padding: style.horizontalTitleGap,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                item.title,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle()
                                    .merge(
                                        Theme.of(context).textTheme.subtitle1)
                                    .merge(style.titleStyle)
                                    .merge((unusualTitleStyle ??
                                        (int index) {
                                          return const TextStyle();
                                        })(index)),
                              ),
                              item.subTitle != null
                                  ? SizedBox(
                                      height: style.verticalTitleGap,
                                    )
                                  : const SizedBox.shrink(),
                              item.subTitle != null
                                  ? Padding(
                                      padding: EdgeInsets.only(
                                          top: style.verticalTitleGap ?? 0),
                                      child: Text(
                                        item.subTitle!,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: const TextStyle()
                                            .merge(Theme.of(context)
                                                .textTheme
                                                .subtitle2)
                                            .merge(style.subtitleStyle),
                                      ),
                                    )
                                  : const SizedBox.shrink(),
                            ],
                          ),
                        ),
                      ),
                      // 靠右图标
                      IntrinsicWidth(
                        child: AbsorbPointer(
                          absorbing: onTrailingTap == null,
                          child: GestureDetector(
                            onTap: () {
                              if (onTrailingTap != null) {
                                onTrailingTap!(index);
                              }
                            },
                            child: Row(
                              children: [
                                (buildTrailing ??
                                    (BuildContext context, int index) {
                                      return // 右边文本
                                          CutWidget(
                                        state: item.moreText != null,
                                        correct: Text(
                                          item.moreText ?? '',
                                          style: const TextStyle()
                                              .merge(style.moreTextStyle),
                                        ),
                                      );
                                    })(context, index),

                                // 向右箭头(默认隐藏)
                                CutWidget(
                                    state: showAllNext || item.toNext,
                                    correct: Icon(
                                      // Icons.arrow_forward_sharp,
                                      Icons.arrow_forward_sharp,
                                      size: style.nextIconSize ??
                                          Theme.of(context)
                                              .primaryIconTheme
                                              .size,
                                      color: Theme.of(context)
                                          .primaryIconTheme
                                          .color,
                                    ))
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  )),
            );
          },
          childCount: data.length,
          findChildIndexCallback: (Key key) {
            // final ValueKey<String> valueKey = key as ValueKey<String>;
            // final String data = valueKey.value;
            // return data.indexOf(data);
          }),
    );

    // childrenDelegate: CustomSliverChildDelegate(
    //     itemBuilder: itemBuilder ??
    //         (BuildContext context, int index) {
    //           final item = LinkItem.fromJson(data[index]);
    //           return ListTile(
    //             onTap: () {
    //               // 如果设置了点击事件

    //               if(item.onTap!=null){
    //                 item.onTap!(index);
    //                 return ;
    //               }
    //               // 如果设置了路由
    //               if(item.url!=null){

    //                 Uri url = Uri.parse(item.url!);

    //                 Navigator.pushNamed(context, url.host,
    //                     arguments: url.queryParameters);
    //               }
    //             },
    //             leading: imageBuild(item.logo,size: item.logoSize ),
    //           );
    //         },
    //     list: []));
  }

  /// 图片处理
  Widget imageBuild(BuildContext context, int index, String? path,
      {double? size, BoxFit? fit}) {
    size ??= 56.$rpx;
    if (leading != null) {
      return leading!(context, index);
    }

    if (path == null || path.isEmpty) {
      return const SizedBox.shrink();
    } else if (path.startsWith('http')) {
      return Image.network(
        path,
        fit: fit,
        width: size,
        height: size,
      );
    } else {
      return Image.asset(
        path,
        fit: fit,
        width: size,
        height: size,
      );
    }
  }
}

/// 样式
class LinkListStyle {
  /// 样式
  const LinkListStyle(
      {this.logoSize,
      this.titleStyle,
      this.subtitleStyle,
      this.moreTextStyle,
      this.horizontalTitleGap,
      this.verticalTitleGap,
      this.nextIconSize});

  ///
  final double? logoSize;

  ///title样式
  ///```dart
  ///TextStyle(
  ///  fontWeight: FontWeight.w400,
  ///  fontSize: 26.$rpx,
  ///  height: 40 / 26,
  ///  color: Config.black,
  /// )
  ///```
  final TextStyle? titleStyle;
  final TextStyle? subtitleStyle;
  final TextStyle? moreTextStyle;

  /// 标题与logo 向右图片之间就间隔
  final EdgeInsetsGeometry? horizontalTitleGap;

  /// title 与subtitle 之间的距离
  final double? verticalTitleGap;
  final double? nextIconSize;

  factory LinkListStyle.fromJson(Map<String, dynamic> json) {
    return LinkListStyle(
      logoSize: json['logoSize'] != null ? json['logoSize'] as double? : null,
      titleStyle:
          json['titleStyle'] != null ? json['titleStyle'] as TextStyle? : null,
      subtitleStyle: json['subtitleStyle'] != null
          ? json['subtitleStyle'] as TextStyle?
          : null,
      moreTextStyle: json['moreTextStyle'] != null
          ? json['moreTextStyle'] as TextStyle?
          : null,
      verticalTitleGap: json['verticalTitleGap'] != null
          ? json['verticalTitleGap'] as double?
          : null,
      horizontalTitleGap: json['horizontalTitleGap'] != null
          ? json['horizontalTitleGap'] as EdgeInsetsGeometry?
          : null,
      nextIconSize:
          json['nextIconSize'] != null ? json['nextIconSize'] as double? : null,
    );
  }
}

/// jsonToModel
class LinkItem {
  const LinkItem({
    this.logo,
    required this.title,
    this.subTitle,
    this.moreText,
    this.toNext = false,
    this.url,
    this.onTap,
    this.nextIcon,
  });
  final String? logo;
  final String title;
  final String? subTitle;
  final String? moreText;
  final bool toNext;
  final String? url;
  final Icon? nextIcon;
  final Function()? onTap;

  factory LinkItem.fromJson(Map<String, dynamic> json) {
    assert(json['title'] != null, 'title属性不能为空');
    return LinkItem(
        logo: json['logo'] != null ? json['logo'] as String : null,
        title: json['title'] as String,
        subTitle: json['subTitle'] != null ? json['subTitle'] as String : null,
        moreText: json['moreText'] != null ? json['moreText'] as String : null,
        url: json['url'] != null ? json['url'] as String : null,
        onTap: json['onTap'] != null ? json['onTap'] as Function()? : null,
        nextIcon: json['nextIcon'] != null ? json['nextIcon'] as Icon? : null,
        toNext: json['toNext'] ?? false);
  }
}

class CustomSliverChildDelegate extends SliverChildDelegate {
  final List list;
  final bool keepAlive;
  final Widget Function(BuildContext context, int index) itemBuilder;
  const CustomSliverChildDelegate(
      {required this.list, this.keepAlive = true, required this.itemBuilder});

  /// 根据index构造child
  @override
  Widget build(BuildContext context, int index) {
    // KeepAlive将把所有子控件加入到cache，已输入的TextField文字不会因滚动消失
    // 仅用于演示
    return KeepAlive(keepAlive: keepAlive, child: itemBuilder(context, index));
  }

  /// 决定提供新的childDelegate时是否需要重新build。在调用此方法前会做类型检查，不同类型时才会调用此方法，所以一般返回true。
  @override
  bool shouldRebuild(SliverChildDelegate oldDelegate) {
    return true;
  }

  /// 提高children的count，当无法精确知道时返回null。
  /// 当 build 返回 null时，它也将需要返回一个非null值
  @override
  int get estimatedChildCount => list.length;

  /// 预计最大可滑动高度，如果设置的过小会导致部分child不可见，设置报错
  @override
  double estimateMaxScrollOffset(int firstIndex, int lastIndex,
      double leadingScrollOffset, double trailingScrollOffset) {
    return 2500;
  }

  /// 完成layout后的回调，可以通过该方法获取即将渲染的视图树包括哪些子控件
  @override
  void didFinishLayout(int firstIndex, int lastIndex) {
    //print('didFinishLayout firstIndex=$firstIndex firstIndex=$lastIndex');
  }
}
