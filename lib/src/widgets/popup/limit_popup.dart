/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-11-12 10:43:06
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-12 10:50:13
 */

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../utils/adapter.dart';

class LimitPopup extends StatelessWidget {
  final List<Widget> children;
  final Color? backgroundColor;

  /// 弹框高度自适应
  const LimitPopup({Key? key, required this.children, this.backgroundColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return UnconstrainedBox(
      child: Container(
        color: backgroundColor ?? Theme.of(context).dialogTheme.backgroundColor,
        child: SafeArea(
          child: SizedBox(
            width: context.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: children,
            ),
          ),
        ),
      ),
    );
  }
}
