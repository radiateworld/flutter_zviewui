/*
 * @Descripttion: 文本项， 左右布局
 * @version: v0.0.1
 * @Author: 黄志勇
 * @Date: 2021-08-10 10:12:12
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-12-21 11:25:00
 */


import '../utils/adapter.dart';
import 'package:flutter/material.dart';

class ListItem extends StatelessWidget {
  final Color? color;
  final double? maxHeight;
  final double? minHeight;
  final String title;
  final List<Widget> children;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final TextStyle? titleStyle;

  /// 这是控制左右排列布局的文本属性(默认高度98)
  ///
  /// 例子
  /// texts: <Map>[
  ///    const {
  ///       "text": "Credit gold",
  ///        "children": [
  ///              {
  ///                  "text": "5".price,
  ///                    "style": TextStyle(color: Config.theme)
  ///               },
  ///                {"text": " Optional"}
  ///                ]
  ///          },
  ///         {
  ///             "type": 'icon',
  ///             "icon": Icons.arrow_forward,
  ///              "size": Adapter.rpx(32),
  ///               "color":selgold ? Config.theme: Config.grey5
  ///         }
  ///       ],
  ///
  final List<Map> texts;
  final void Function()? onTap;

  const ListItem(
      {Key? key,
      this.color,
      this.title = '',
      this.maxHeight,
      this.minHeight,
      this.onTap,
      this.texts = const [],
      this.children = const [],
      this.padding,
      this.titleStyle,
      this.margin})
      : super(key: key);

  static EdgeInsetsGeometry marTop = EdgeInsets.only(top: Adapter.rpx(2));

  @override
  Widget build(BuildContext context) {
    double _height = Adapter.rpx(98);
    return Container(
      constraints: BoxConstraints(
          maxHeight: maxHeight ?? minHeight ?? _height,
          minHeight: minHeight ?? _height),
      color: color,
      margin: margin ?? marTop,
      alignment: Alignment.centerLeft,
      padding: padding ?? EdgeInsets.all(Adapter.rpx(32)),
      child: InkWell(
        onTap: onTap,
        child: Row(
          children: [
            ...title.bigTitle(context, titleStyle: titleStyle),
            ...texts.toBuild(context),
            ...children
          ],
        ),
      ),
    );
  }
}

extension ItemString on String {
  List<Widget> bigTitle(BuildContext context, {TextStyle? titleStyle}) {
    List titleList = split('<==>');
    List<Widget> template = [];
    if (this == '') {
      return template.toList();
    }
    for (int i = 0; i < titleList.length; i++) {
      template.add(
        Expanded(
          flex: i == 0 ? 1 : 0,
          child: Text(
            titleList[i],
            style: titleStyle ?? Theme.of(context).textTheme.headline1,
          ),
        ),
      );
    }

    return template.toList();
  }
}

extension ItemList on List<Map> {
  toBuild(BuildContext context) {
    List<Widget> template = [];
    for (int i = 0; i < length; i++) {
      Map obj = this[i];
      late Widget widget;
      switch (obj['type']) {
        case 'text':
          widget = Expanded(
            flex: i == 0 ? 1 : 0,
            child: obj['child'] ?? Text(obj['text']),
          );
          break;
        case 'icon':
          widget = Expanded(
            flex: i == 0 ? 1 : 0,
            child: obj['child'] ??
                Icon(
                  const IconData(0xe63b, fontFamily: 'iconfont'),
                  color: obj['color'] ?? Theme.of(context).primaryIconTheme.color,
                  size: obj['size'] ?? Adapter.rpx(30),
                ),
          );
          break;
        default:
          widget = Expanded(
            flex: i == 0 ? 1 : 0,
            child: RichText(
              text: TextSpan(
                style: obj['style'] ??
                    TextStyle(
                        fontSize: Adapter.rpx(24),
                        fontWeight: FontWeight.w400,
                        color: Config.grey3),
                text: '${obj['text']}     ',
                children: obj['childrenWidget'] ??
                    [
                      ...(obj['children'] ?? []).map((item) {
                        return TextSpan(
                          text: item['text'],
                          style: item['style'] ??
                               TextStyle(color: Config.black),
                        );
                      }).toList()
                    ],
              ),
            ),
          );
      }
      template.add(widget);
    }
    // //print(template);
    return template.toList();
  }
}
