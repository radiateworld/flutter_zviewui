/*
 * @Descripttion: 
 * @version: v1.0.0
 * @Author: 黄志勇
 * @Date: 2021-10-14 20:38:15
 * @LastEditors: 黄志勇
 * @LastEditTime: 2021-11-15 16:58:16
 */

import 'package:flutter/material.dart';
import 'package:zviewui/src/utils/is_net_resource.dart';

/// 重叠方向
enum OverlapImageType {
  /// 左边在下面()
  left,

  /// 右边在下面
  right
}

/// 重叠图片部件
class OverlapImage extends StatelessWidget {
  /// 图片的尺寸
  final double size;

  /// 图片的数组（一般是网络图片）
  final List<String> list;

  /// 当外部尺寸固定是设定该值
  final double? maxWidth;

  /// 重叠的尺寸，应该小于size.width
  final double? offset;

  /// OverlapImageType.left 左边的在下面(默认)
  ///
  /// OverlapImageType.right 右边的在下面
  final OverlapImageType type;

  /// 图片边框
  final BoxBorder? border;

  /// 重叠部件外边距
  final EdgeInsetsGeometry? margin;

  /// 重叠部件内边距
  final EdgeInsetsGeometry? padding;

  /// 重叠图片部件
  /// 没达到预期效果添加offset 或者maxWidith 属性
  const OverlapImage(
      {Key? key,
      required this.list,
      required this.size,
      this.offset,
      this.maxWidth,
      this.border,
      this.type = OverlapImageType.left,
      this.margin,
      this.padding})
      : assert(offset == null || (offset - size < 0), '警告:offset应小于size的宽度'),
        assert(
            (offset == null && maxWidth != null) ||
                (offset != null && maxWidth == null) ||
                (offset == null && maxWidth == null),
            '警告:maxWidth与offset不能同时设置'),
        super(key: key);

  double countWidth() {
    final len = list.length;
    if (offset == null) {
      // return 750.$rpx;
      return double.infinity;
    }
    // //print('444444444444444444444444444');
    return len * size - (len - 1) * offset!;
  }

  /// 构造图片列表
  List<Widget> listBuild(
      {required BuildContext context, required BoxConstraints constraints}) {
    List<Widget> template = [];
    double widgetOffset = 0;
    // 如果图片无法重叠
    if (size * list.length < constraints.maxWidth) {
      widgetOffset =
          (constraints.maxWidth - size * list.length) / (list.length - 1) +
              size;
    } else {
      // 图片重叠
      widgetOffset = size -
          (size * list.length - constraints.maxWidth) / (list.length - 1);
    }
    for (int i = 0; i < list.length; i++) {
      var imgStr =
          OverlapImageType.right == type ? list[list.length - i - 1] : list[i];
      template.add(
        Positioned(
          left: OverlapImageType.left == type ? i * widgetOffset : null,
          right: OverlapImageType.right == type ? i * widgetOffset : null,
          child: Container(
            width: size,
            height: size,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(size / 2),
              border: border ?? Border.all(color: Colors.white, width: 1),
              image: isNetResource(imgStr)
                  ? DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(imgStr),
                    )
                  : DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(imgStr),
                    ),
            ),
          ),
        ),
      );
    }
    return template.toList();
  }

  @override
  Widget build(BuildContext context) {
    return UnconstrainedBox(
      child: Container(
        constraints: BoxConstraints(
          maxWidth: maxWidth ?? countWidth(),
        ),
        padding: padding,
        margin: margin,
        height: size,
        color: Theme.of(context).primaryColor,
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Stack(
              children: listBuild(context: context, constraints: constraints),
            );
          },
        ),
      ),
    );
  }
}
