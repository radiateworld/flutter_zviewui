import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:zviewui/zviewui.dart';

void main() {
  const MethodChannel channel = MethodChannel('zviewui');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });
  // 初始化的代码
  // test('getPlatformVersion', () async {
  //   expect(await Zviewui.platformVersion, '42');
  // });
}
